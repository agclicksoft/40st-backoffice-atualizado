<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
	<head>
		<title>40street</title>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	</head>
	<body>
		<div align="center">
      		<div style="text-align: center;"></div>
			<table border="0" cellspacing="0" width="800" height="770" background="http://40st-backoffice.webbyapp.com/app/webroot/img/mail/background_mail.png">
				<thead>
					<tr>
						<td>				
							<?php echo $this->Html->image('mail/header_mail_welcome2.png', array('fullBase' => true, 'style' => "padding: 0 0 0 32px;")); ?>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div style=" padding: 0 90px;">
							
								<p style="font-size: 16px; color: #000000; margin:15px 0;">Bem-vindo ao aplicativo 40street!</p>
								<p style="font-size: 16px; color: #000000; margin:15px 0;text-align:justify;">Para finalizar seu cadastro é preciso clicar no link abaixo:</p>
								<p style="font-size: 16px; color: #000000; margin:15px 0;">
									<?php echo $this->Html->link ('>> Confirme aqui seu cadastro <<', array ('controller' => 'Clients', 'action' => 'sucess', $id_user,'?' => array('origem' => $origem), 'full_base' =>true)); ?>
								</p>
								<p style="font-size: 14px; color: #000000; margin:15px 0;text-align:justify;">Caso não tenha efetuado este cadastro, <?php echo $this->Html->link ('clique aqui', array ('controller' => 'Clients', 'action' => 'delete', $id_user, 'full_base' =>true )); ?>.</p>
								<p style="font-size: 18px; color: #000000; margin:15px 0;">Atenciosamente,<br>Equipe 40street.</p>
							</div>
						</td>
					</tr>
				</tbody>
				<tfoot>	
					<tr>
						<td>									
							<?php echo $this->Html->image('mail/footer_mail3.png', array('fullBase' => true , 'style' => "padding: 0 0px 32px 32px;")); ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</body>
</html>