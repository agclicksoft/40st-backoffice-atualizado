<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
	<head>
		<title>40street</title>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	</head>
	<body>
		<div align="center">
      		<div style="text-align: center;"></div>
			<table backgound-size="800px 1000px" border="0" cellspacing="0" width="800" height="1000" background="http://40st-backoffice.webbyapp.com/app/webroot/img/mail/background_mail3.png">
				<thead>
					<tr>
						<td>				
							<?php echo $this->Html->image('mail/header_mail_confirm.png', array('fullBase' => true, 'style' => "padding: 0 0 0 32px;")); ?>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div style="padding: 0 90px;">
								
								<p style="font-size: 1em; color: #000000; margin:15px 0;">Oi <?php echo $nome; ?>,</p>
								<p style="font-size: 1em; color: #000000; margin:15px 0;text-align:justify;">Bem-vindo(a) à comunidade 40street, você foi selecionado(a) a ser um dos primeiros a degustar nosso app de relacionamento 40street. Queremos que se sinta em casa, afinal, agora 40street é a sua rua também.</p>
								<p style="font-size: 1em; color: #000000; margin:15px 0;text-align:justify;">40street é o novo aplicativo on-line, construído exclusivamente para pessoas acima de 40 anos. Você terá a oportunidade de conhecer pessoas bacanas, com interesses em comum na vida real, que dividem o seu estilo de vida e buscam encontrar um novo amor.</p>
								<p style="font-size: 1em; color: #000000; margin:15px 0;text-align:justify;">Obrigado pelo seu cadastro no App 40street. Nossa equipe está trabalhando a todo vapor, criando a ferramenta certa para encurtar a distância entre você e aquela pessoa especial.</p>
								<p style="font-size: 1em; color: #000000; margin:15px 0;text-align:justify;">Em breve será fácil e divertido baixar e utilizar o App 40street.</p>
								<p style="font-size: 1em; color: #000000; margin:15px 0;text-align:justify;">Até lá!</p>
								<p style="font-size: 1em; color: #000000; margin:15px 0;text-align:justify;">Equipe 40street. <div style="float: right; top: -50px;">
								<a href="https://www.facebook.com/fortystreet"><img style="width:30px;margin-right: 10px;" src="http://40st-backoffice.webbyapp.com/app/webroot/img/mail/face.png"></a>
								<a href="https://instagram.com/fortyst/"><img style="width:30px;margin-right: 10px;" src="http://40st-backoffice.webbyapp.com/app/webroot/img/mail/insta.png"></a>
								<a href="http://fortyst.tumblr.com/"><img style="width:30px;margin-right: 10px;" src="http://40st-backoffice.webbyapp.com/app/webroot/img/mail/tumb.png"></a>
								<a href="https://www.pinterest.com/40street0271/"><img style="width:30px" src="http://40st-backoffice.webbyapp.com/app/webroot/img/mail/pin.png"></a></div>
							</div></p>
								
						</td>
					</tr>
				</tbody>
				<tfoot>	
					<tr>
						<td>			
							<?php echo $this->Html->image('mail/footer_mail3.png', array('fullBase' => true, 'style' => "padding: 0 0px 32px 32px;")); ?>				
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</body>
</html>
