<script language='JavaScript'>
function SomenteNumero(e){
var tecla=(window.event)?event.keyCode:e.which;
if((tecla>47 && tecla<58)) return true;
else{
if (tecla==8 || tecla==0) return true;
else return false;
}
}
</script>

<script type="text/javascript">
	
$(document).ready(function(){
  $("#limpar").click(function(){
  
	  $('#SearchFacebookId1').removeAttr('checked');
	  $('#SearchFacebookId2').removeAttr('checked');
	  $('#SearchFacebookId3').removeAttr('checked');
	  
	  $('#SearchGender1').removeAttr('checked');
	  $('#SearchGender2').removeAttr('checked');
	  $('#SearchGender3').removeAttr('checked');

	  $('#SearchName').val("");
	  $('#SearchEmail').val("");
	  $('#SearchBirthday').val("");
	  
	  $('#SearchCountryId').val("0");
	  $('#SearchStateId').val("0");

  });    

});

</script>

<div class="row">    
  <div class="span12 sessao">
  	<h2> Usuários</h2>	

  <?php echo $this->Form->create('Search',array( 'url' => array('controller' => 'user_backs', 'action' => 'index')));?>        
          
      <div class="column-left">
            <fieldset>
               <p>
               <?php  echo $this->Form->input('name', array('label' => 'Nome do Usuário','required' => 'false', 'div' => false, 'class' => "text-input large-input")); ?> 
               </p>                              
                <p>                    
		            <?php  echo $this->Form->input('email', array('label' => 'Email', 'required' => 'false', 'div' => false, 'class' => "text-input large-input")); ?>
		        </p>
		        <p> 

		        	<label for="Defintion2">Sexo:</label>                   
		           <?php 
		           echo $this->Form->input('gender', array(
					 'div' => true,
					 'label' => true,
					 'checked' => false,
					 'type' => 'radio',
					 'legend' => false,
					 'options' => array(1 => 'Masculino ', 2 => 'Feminino', 3 => 'Todos')
					));         
				   ?>
		          </p>
	              
	              <?php 
	              	$options = array
	                  (
	                  'label' => 'Buscar',
                      'id' => true,
	                  'class' => 'btn  btn-small btn-success btn-search',
	                  'div' => false

	                  );
	              	echo $this->Form->submit('Buscar', $options); ?>                   
	              

	             <input type="button" id="limpar" class="btn  btn-small btn-success btn-search"  value="Limpar">
	         </fieldset>
        </div> 
        <div class="column-left">
	        <fieldset>
	        	<p>                    
		            <?php
		              echo $this->Form->input('country_id',array('label' => 'País','required' => 'false', 'div' => false,'empty' => array('0' => 'Escolha uma das opções')));					 
		              ?>
		          </p>	         
		          <p>     
		          	 <?php               
		             echo $this->Form->input('state_id',array('label' => 'Estado','required' => 'false', 'div' => false,'empty' => array('0' => 'Escolha uma das opções')));
		             ?>
		          </p>	
		          <p>    
		          	<label for="Definition">Cadastro por:</label>                 
		             <?php 
			            //$options = array('1' => 'Login e Senha', '2' => 'Facebook');
						//$attributes = array('legend' => false, 'div' => true', label' => true);
						//echo $this->Form->radio('facebook_id', $options, $attributes);			

						echo $this->Form->input('facebook_id', array(
						 'div' => true,
						 'label' => true,
						 'type' => 'radio',
						 'legend' => false,
						 'options' => array('1' => 'Login e Senha', '2' => 'Facebook', 3 => 'Todos')
					));  			       
					  ?>
		          </p>
	        </fieldset>  
        </div>  
        <div class="column-left">
	        <fieldset>
		          <p>                    
		            <?php  echo $this->Form->input('birthday', array('label' => 'Idade','required' => 'false', 'div' => false, 'style'=>'width:50px', 'onkeypress' => 'return SomenteNumero(event)')); ?>
		        </p>	        
		                   	        
		          
	        </fieldset>  
        </div>  	

              <?php 
                  
                   echo $this->Form->end();?> 
 </div>

	<div class="span12">

		<table class="table">
	        
	       <thead>  
			<tr>
				<th>Nome</th>
				<th>Email</th>
				<th>Idade</th>
				<th>Sexo</th>
				<th>País</th>
		    	<th>Estado</th>
		    	<th>Cadastrado por</th>  
		    	<th>Cadastrado em</th>     
			</tr>
		   </thead>	
			<!-- Aqui é onde nós percorremos nossa matriz $posts, imprimindo
			as informações dos posts -->
		  
		   	<?php if(count($users)): ?>
			   	<tbody>
					<?php foreach ($users as $user): ?>
						<tr>
							<td><?php echo $user['User']['name']; ?></td>
							<td><?php echo $user['User']['email']; ?></td>					
							<td><?php echo $this->Age->get_age($user['User']['birthday']); ?></td>
							<td>
							<?php 
							
							if ($user['User']['gender'] == 1)
							{
							   echo 'Masculino';
							}else{
		                            echo 'Feminino'; 
							     };
							?>
							</td>
							<td><?php echo $user['Country']['name']; ?></td>			
							<td><?php echo $user['State']['name']; ?></td>
							<td>
							<?php

							if ($user['User']['facebook_id'] == null)
							{
								echo 'Login e senha';
							}else{
		                          echo 'Facebook';
							 }

		                    ?>
							</td>
							<td>
								<?php echo date('d/m/Y H:i:s', $timestamp = (strtotime($user['User']['created'])))?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
	    			<tr>
	      				<td colspan="8">
	        				<div class="pagination">
								<?php 
									echo $this->Paginator->prev('<<', null, null, array('class' => 'desabilitado'));
									echo $this->Paginator->numbers();
									echo $this->Paginator->next('>>', null, null, array('class' => 'desabilitado')); 
								?>
							</div>
	        				<div class="clear"></div>
	  					</td>
					</tr>
				</tfoot>
			<?php else: ?>
				<tbody>
					<tr>
						<td colspan="8" style="text-align:center;">Nenhum usuários cadastrado.</td>
					</tr>
				</tbody>
			<?php endif; ?>		  
		</table>
	 </div>	
</div>