﻿<div class="content" style="position:relative;">
  <div class="midle">
    <div class="introduction">
      <!--Cadastra-se e seja um dos primeiros a testar o nosso novo aplicativo social. Conheça pessoas novas, descubra o amor e divirta-se!-->
      <div class="text-main" style="font-weight:bold;">Fa&ccedil;a seu cadastro e seja um dos primeiros a testar o nosso novo aplicativo social. Conhe&ccedil;a pessoas novas e divirta-se.</div>
      <div style="font-weight:bold;">Encontre-se na 40street.</div>
    </div>
  </div>

  <div class="form">
    <?php echo $this->Form->create(null, array('url' => array('controller' => 'clients', 'action' => 'add'))); ?>

      <?php echo $this->Form->input('id', array('type' => 'hidden','name' => 'data[User][id]')); ?>
      
      <?php if(isset($facebook_id)): ?>  
        <?php echo $this->Form->input('facebook_id', array('type' => 'hidden', 'value' => isset($facebook_id) ? $facebook_id : null)); ?>
        <?php echo $this->Form->input('facebook_token', array('type' => 'hidden', 'value' => isset($facebook_token) ? $facebook_token : null)); ?>
      <?php endif; ?>
      
      <div class="margin"> 
        <div class="name margin-mobile">
          <div class="label">Nome: </div>
          <?php echo $this->Form->input('first_name', array('label' => false, 'div' => false, 'onchange' => 'enablesubmit();', 'class' => 'input input-small', 'value' => isset($first_name) ? $first_name : null));?>  
            
          <?php
            if ($this->Check->check_user_agent('mobile'))        
              echo '<span id="error_name1" style="display:none;" class="error">Preencha o campo com o seu nome.</span>';
          ?>
        </div>
        <div class="last_name margin-mobile">
          <div class="label label-rigth">Sobrenome: </div>
          <?php echo $this->Form->input('last_name', array('label' => false,'div' => false, 'onchange' => 'enablesubmit();', 'class' => 'input input-small', 'value' => isset($last_name) ? $last_name : null));?>    

          <?php
            if ($this->Check->check_user_agent('mobile'))        
              echo '<span id="error_name2" style="display:none;" class="error">Preencha o campo com o seu sobrenome.</span>';
          ?>
        </div>
        <?php
          if (!$this->Check->check_user_agent('mobile'))        
            echo '<span id="error_name1" style="display:none;" class="error">Preencha o campo com o seu nome.</span> <span id="error_name2" style="display:none;" class="error">Preencha o campo com o seu sobrenome.</span>';
        ?>  
       
      </div> 
      
      <div class="email margin margin-mobile">
        <div class="label">E-mail: </div>
        <?php echo $this->Form->input('email', array('label' => false,'div' => false,'onchange' => 'enablesubmit();', 'class' => 'input input-large', 'value' => isset($email) ? $email : null));?>
        <span id="error_email" style="display:none;" class="error">Preencha o campo com e-mail v&aacute;lido.</span>
      </div>

      <div class="help_image">
        <?php if (!isset($facebook_id)): ?>
          <div class="user_pass2 margin-mobile">
            <div class="label">Senha: </div>
            <?php echo $this->Form->input('password', array('label' => false,'div' => false,'onchange' => 'enablesubmit();', 'class' => 'input input-medium'));?>
            <span id="error_password" style="display:none;" class="error">A senha deve ter no min&iacute;mo 8 caracteres.</span>
          </div>

          <div class="user_pass margin margin-mobile">
            <div class="label">Confirmar Senha: </div>
            <?php echo $this->Form->input('password_confirm', array('type' => 'password','label' => false,'div' => false, 'onchange' => 'enablesubmit();', 'class' => 'input input-medium'));?>
            <span id="error_verify" style="display:none;" class="error">As duas senhas n&atilde;o conferem.</span>
          </div>
        <?php endif; ?>

        <div class="user_age margin margin-mobile">
          <div class="label">Nascimento: </div>
          <?php 
            echo $this->Form->input('birthday', array('type' => 'text','label' => false,'div' => false, 'placeholder' => 'Exemplo: 31/12/2015','maxlength' => 10, 'onchange' => 'enablesubmit();', 'class' => 'input input-medium', 'value' => isset($birthday) ? $birthday : null));
          ?>
          <span id="error_birthday" style="display:none;" class="error">Para registra-se tem que ter 40 anos ou mais.</span>
        </div>

        <div class="user_from margin margin-mobile">
          <div class="label">Pa&iacute;s: </div>
          <?php
            echo $this->Form->input('country_id', array('label' => false,'empty' => 'Selecionar um País', 'type' => 'select','options' => $countries,'onchange' => 'enablesubmit();' , 'class' => 'input-select select-small'));
          ?>
          <div class="label state-rigth">Estado: </div>
          <?php
            echo $this->Form->input('state_id', array('label' => false,'empty' => 'Selecionar um Estado', 'type' => 'select','options' => $states,'onchange' => 'enablesubmit();' , 'class' => 'input-select select-small'));
          ?>
        </div>

        <div class="option margin margin-mobile">
          <div class="label">Sexo:</div>    
          <?php echo $this->Form->input('gender', array('type' => 'hidden')); ?>
          <div id="radios">
            <div id="radio-1" class="radios" onclick="selectRadio(1);"></div>
            <div class="radio-text" onclick="selectRadio(1);">Masculino</div>
            <div id="radio-2" class="radios" onclick="selectRadio(2);"></div>
            <div class="radio-text" onclick="selectRadio(2);">Feminino</div>
            <!--div id="radio-3" class="radios" onclick="selectRadio(3);"></div>
            <div class="radio-text" onclick="selectRadio(3);">Outros</div-->
          </div>
        </div>
        <div class="condition margin ">
          <div class="label"></div>
          <b>        
            <?php //echo $this->Form->input('agree', array('type' => 'hidden')); ?>
            <!--div id="checks">
              <div id="check-1" class="checks" onclick="selectCheck(1);"></div>
              <div class="check-text" onclick="selectCheck(1);">Termo de Uso</div>
            </div-->
          </b>  
        </div>
      </div>
      <div class="image_use">
        <?php echo $this->Html->image('imagem_celular3.png'); ?>
      </div>
    <?php 
      $options = array('label' => 'CADASTRAR', 'id' => 'save_id', 'disabled' => 'disabled', 'onchange' => 'enablesubmit();');
      echo $this->Form->end($options);
    ?>  
	
	<div class="text-main text-man-mobile" style="color: #d90305;position:Relative; top:-25px;">*Seus dados pessoais não serão divulgados</div>
 
 </div>

<div id="pop" style=" display: <?php echo isset($save) && $save == 1 ? 'table' : 'none'; ?>; ">
  <div id="pop-second" >
    <div id="pop-content">
      <p id="text-main" style="line-height: 43px; text-align: center; color:#484848;margin: 22px 0 0 0; font-weight: bold;">
        Enviaremos confirma&ccedil;&atilde;o para o e-mail informado.
      </p>
      <p id="text-second">
        (Favor verificar  sua lista de spam)
      </p>

      <div id="botao-ok" onclick="<?php echo $this->Check->check_user_agent('mobile') ? 'sendMessageMobile()' : 'sendMessage()'; ?>">OK</div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  selectRadio(<?php echo $gender; ?>)
</script>
