<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
	<head>
		<title>40STREET</title>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	</head>
	<body>
		<div align="center">
      		<div style="text-align: center;"></div>
			<table border="0" cellspacing="0" width="800" height="770" background="http://localhost:8080/app/webroot/img/mail/background_mail.png">
				<thead>
					<tr>
						<td>				
							<?php echo $this->Html->image('mail/header_mail_welcome.png', array('fullBase' => true, 'style' => "margin: 0 11px 0px; float: right;")); ?>
						</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div style="margin: 0 90px;">
								<p style="font-size: 18px; margin: 15px 0;">Bem-vindo ao aplicativo 40Street!</p>
								<p style="font-size: 18px; margin: 15px 0;">Para finalizar seu cadastro é preciso clicar no link abaixo:</p><br/>
								<p style="font-size: 18px; margin: 15px 0;">
									<?php //echo $this->Html->link ('Confirmar!', array ('controller' => 'Clients', 'action' => 'sucess', $id_user ,'full_base' =>true )); ?>
								</p><br/>
								<p style="font-size: 18px; margin: 15px 0;">Caso não tenha efetuado este cadastro, clique aqui!</p>
								<p style="font-size: 18px; margin: 15px 0;">Obrigado,<br>Equipe 40STREET.</p>
							</div>
						</td>
					</tr>
				</tbody>
				<tfoot>	
					<tr>
						<td>			
							<?php echo $this->Html->image('mail/footer_mail.png', array('fullBase' => true, 'style' => "margin: 0 11px 32px; float: right;")); ?>				
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</body>
</html>