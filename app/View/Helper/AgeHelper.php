<?php
 
App::uses('AppHelper', 'View/Helper');
 
class AgeHelper extends AppHelper {
	 public $helpers = array('Html');
	
	function get_age($check)
	{
	    $from = new DateTime($check);
		$to   = new DateTime('today');
		echo $from->diff($to)->y;
	}
}
?>