<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
  <?php echo $this->Html->charset(); ?>
  <title>
    40STREET
  </title>
  <?php
    echo $this->Html->meta('icon');   
    echo $this->Html->css('bootstrap.min');
    echo $this->Html->css('bootstrap-responsive.min');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
   ?>

  <style type="text/css">
    body {
      padding-top: 60px;
      padding-bottom: 40px;
    }
    .navbar-inner{
      background-color: #58595b;
      background-image: none !important;
      border: none !important;
    }
    .navbar .nav>li>a{
      color: #58595b !important;
      text-shadow: none !important;
    }

    .navbar {
      color: #fff !important;
      text-shadow: none !important;
    }

    .navbar .divider-vertical{
      border-right: none !important; 
      border-left: none !important;
    }

    .menu{
      width: 1208px !important;
    }

    li {
      line-height: 32px !important;
    }

    .navbar .nav {      
      margin: 0px !important;
      width: 90% !important;
    }

    .right{
      float:right !important;
    }

    .sessao{
      background-color: transparent !important;
      padding: 0 10px 0 0px !important;
    }
    .btn-success:hover, .btn-success:active, .btn-success.active, .btn-success.disabled, .btn-success[disabled], .btn-success, .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary.active, .btn-primary.disabled, .btn-primary[disabled], .btn-danger,.btn-danger:hover, .btn-danger:active, .btn-danger.active, .btn-danger.disabled, .btn-danger[disabled]{
      background-color: #ed1b24 !important;
      background-image: none !important;
      border-radius: 0;
      border: none;
    }

    .container{
      width: 1156px;
    }

    div.questions{
      background-color: #ececec;
      border: none;
      padding: 0px;
      width: 1000px;
    }

    .color{
      color: #fff;
    }
    .message
    {
      background-color: #f2dede;
      border-color: #eed3d7;
      color: #b94a48;
      border-radius: 4px;
      margin-bottom: 20px;
      padding: 8px 35px 8px 14px;
      text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5)
    }         
  </style>
</head>
<body>
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container" >
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        <a href="#" class="brand"><?php echo $this->Html->image('logo.png', array('style' => "height: 40px;")); ?></a>
      </div>
    </div>
  </div>

  <div class="container" style=" margin-top: 80px; ">
    <div class="hero-unit" >
      <?php echo $this->Session->flash(); ?>
      <?php echo $this->fetch('content'); ?>
    </div>
    <footer>
      <p></p>
    </footer>
  </div>
</body>
</html>