<!DOCTYPE html>
<html>
<head>
<title>40STREET</title>
<?php echo $this->Html->charset(); ?>
<?php echo $this->Html->script('jquery');?>
<?php
  echo $this->Html->meta('icon');
  echo $this->Html->css('bootstrap.min');
  echo $this->Html->css('bootstrap-responsive.min');
  echo $this->Html->css('admin');
  echo $this->Html->css('colorpicker');
  echo $this->Html->css('layout');
  echo $this->fetch('meta');
  echo $this->fetch('css');
  echo $this->fetch('script');
?>
  <style type="text/css">
    body 
    {
      padding-top: 60px;
      padding-bottom: 40px;
    }
    .navbar-inner{
      background-color: #58595b;
      background-image: none !important;
      border: none !important;
    }
    .navbar .nav>li>a{
      color: #fff !important;
      text-shadow: none !important;
    }

    .navbar {
      color: #fff !important;
      text-shadow: none !important;
    }

    .navbar .divider-vertical{
      border-right: none !important; 
      border-left: none !important;
    }

    .menu{
      width: 1208px !important;
    }

    li {
      line-height: 32px !important;
    }

    .navbar .nav {      
      margin: 0px !important;
      width: 90% !important;
    }

    .right{
      float:right !important;
    }

    .sessao{
      background-color: transparent !important;
      padding: 0 10px 0 0px !important;
    }
    
    .btn-success:hover, .btn-success:active, .btn-success.active, .btn-success.disabled, .btn-success[disabled], .btn-success, .btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary.active, .btn-primary.disabled, .btn-primary[disabled], .btn-danger,.btn-danger:hover, .btn-danger:active, .btn-danger.active, .btn-danger.disabled, .btn-danger[disabled]{
      background-color: #58595b !important;
      background-image: none !important;
      border-radius: 0;
      border: none;
     
    }

    .container{
      width: 1156px;
    }

    div.questions{
      background-color: #ececec;
      border: none;
      padding: 0px;
      width: 1000px;
    }

    .color{
      color: #fff !important;
    }

    .block_color{
       background-color: #e8e5bf;
    }

    .control-group{
      display: inline-block;
    }

    .message
    {
      background-color: #cccccc;
      border-color: #eed3d7;
      color: #000;
      border-radius: 4px;
      margin-bottom: 20px;
      padding: 8px 35px 8px 14px;
      text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5)
    }

    .error-message
    {
      color: red;
    }
</style>
</head>

<body>

  <div class="overload" style="display:none;"></div>
    <div id="popup" style="display:none"> 
      <div id="popup_content">
      <div id="content"></div>
    </div>
  </div>  
     
  <div class="overload_tree_viewer" style="display:none;"></div>
  <div id="popup_tree_viewer" style="display:none">    
    <div id="popup_content_tree_viewer" >
      <div id="content_tree_viewer" ></div>  
    </div>
  </div>  
             
  <?php if($this->Session->flash('auth')){ ?>
    <p class="alert alert-notice"><?php echo $this->Session->flash('auth'); ?></p>
  <?php } ?>      

  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container menu">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>  
          <a href="#" class="brand"><?php echo $this->Html->image('logo.png', array('style' => "height: 40px;")); ?></a>              
          <div class="nav-collapse">
              <ul class="nav">
                <li><?php echo $this->Html->link('Administradores',array('controller' => 'admins', 'action' => 'index') ) ?></li>
                <li><?php echo $this->Html->link('Denunciados',array('controller' => 'reports', 'action' => 'denuncia') ) ?></li>
                <li><?php echo $this->Html->link('Usuários',array('controller' => 'user_backs', 'action' => 'index')) ?></li>
                 <li class="right"><?php echo $this->Html->link('Logout',array('controller' => 'admins', 'action' => 'logout') ) ?></li>       
              </ul>       
          </div>
      </div>
    </div>
  </div>

  <div class="container" style=" margin-top: 20px; ">
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->fetch('content'); ?>
  </div>

  <?php echo $this->Js->writeBuffer();?>

</body>

<footer style="text-align: right; margin-right: 20px;">
  <p>&copy; Desenvolvido por Clicksoft</p>
</footer>

</html>