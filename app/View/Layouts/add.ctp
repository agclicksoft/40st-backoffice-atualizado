<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<title>
			40STREET
		</title>
		<?php			

		        echo $this->fetch('meta');
		    	//echo $this->fetch('css');
			    //echo $this->fetch('script');

			    echo $this->Html->script('jquery');
				echo $this->Html->script('jquery.maskedinput.min');
				echo $this->Html->script('utils');
				echo $this->Html->script('frontend');
				echo $this->Html->script('validate');

                if ($this->Check->check_user_agent('mobile'))
                {
                	echo $this->Html->css('bootstrap/css/bootstrap.min.css');
		        	echo $this->Html->css('layout-mobile');
		        	echo $this->Html->css('frontend_mobile');
		        }
		        else
		        	echo $this->Html->css('frontend');
		?>
	</head>
	<body>
		<div id="header-mobile"></div>
		<div class="all">
			<div class="content-header">
				<div class="header">
					<div class="image">
						<div class="image_2">
							<div class="image_3">
								<?php echo $this->Html->image('logo.png'); ?>
							</div>
						</div>
					</div>
					<div class="text">
						<div class="header_text">
							<div class="header_text2">
								<span class="pre-cadastro">Cadastro</span>
								<div class="close-botton"><?php echo $this->Html->image('fechar.png'); ?></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="content-message">
				<?php echo $this->Session->flash(); ?>
			</div>

		  	
			<?php echo $this->fetch('content'); ?>
			<?php echo $this->Js->writeBuffer(); ?>
			
			</div>




		</div>
		<div id="footer-mobile"></div>

	</body>
</html>