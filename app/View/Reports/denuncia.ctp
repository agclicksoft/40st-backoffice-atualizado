<div class="row">   
  <div class="span12 sessao">
    <h2> Denunciados</h2>  
    <?php echo $this->Form->create('Search',array( 'url' => array('controller' => 'reports', 'action' => 'denuncia')));?>         
    <div class="column-left">
        <fieldset>
          <p>
           <?php  echo $this->Form->input('reported', array('label' => 'Nome do Denunciado','required' => 'false', 'div' => false)); ?>                  
          </p>
          
        </fieldset>
    </div>   
    <div class="column-left">
          <fieldset>
            <p>
             <?php  echo $this->Form->input('reporter', array('label' => 'Quem Denunciou','required' => 'false', 'div' => false)); ?>                             
            </p>   

          </fieldset>
    </div>     
    <div class="column-left">
      <?php
        echo $this->Form->input('optreport',array('label' => 'Tipo de Denuncia','type' =>'select','multiple' => true,'options' => $optreport));
      ?>
    </div>
    <?php 
      $options = array('label' => 'Buscar', 'class' => 'btn  btn-small btn-success btn-search', 'div' => false);
      echo $this->Form->end($options);
    ?>          
  </div>  
  <div class="span12">
    <table class="table" >
      <thead>
         <tr>         
          <th  style="width:200px">Deunciado</th>
          <th  style="width:200px">Quem Denunciou</th>
          <th  style="width:150px">Tipo de Denuncia</th>
          <th  style="width:400px">Descrição</th>
          <th  style="width:200px">Criado em</th>
          <th  style="width:200px">Modificado em</th>       
        </tr>      
      </thead>
      <?php if(count($reports)): ?>
        <tbody>
          <?php foreach ($reports as $report): ?>
            <tr>
              <?php
                if ($report['People']['situation'] == true)
                {
                  echo "<tr class='block_color'>";
                } 
                else 
                {
                  echo "<tr>";
                }       
              ?>
              <td ><?php echo $report['People']['name']?></td>
              <td ><?php echo $report['User']['name']?></td>
              <td  ><?php echo $report['Optreports']['report']?></td>
              <td ><div style="witdh:20%; word-break: break-all;"><?php echo $report['Report']['description']?></div></td>
              <td><?php echo date('d/m/Y H:i:s', $timestamp = (strtotime($report['Report']['created'])))?></td>
              <td><?php echo date('d/m/Y H:i:s', $timestamp = (strtotime($report['User']['modified'])))?></td>
              <?php 
                if (!$report['People']['situation'] == true)
                {
              ?>
                  <td><?php echo $this->Html->link('Bloquear', array('action' => 'block',$report['People']['id']))?></td>
              <?php 
                }  
                else 
                { 
              ?>
                  <td><?php echo $this->Html->link('Desbloquear', array('action' => 'unblock', $report['People']['id']))?></td>
              <?php 
                }
              ?>
              <td><?php echo $this->Form->postlink('Retirar', array( 'action' => 'delete', $report['Report']['id']))?></td>
            </tr>          
          <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="8">
              <div class="pagination">
                <?php 
                  echo $this->Paginator->prev('<<', null, null, array('class' => 'desabilitado'));
                  echo $this->Paginator->numbers();
                  echo $this->Paginator->next('>>', null, null, array('class' => 'desabilitado')); 
                ?>
              </div>
              <div class="clear"></div>
            </td>
          </tr>
        </tfoot>
      <?php else: ?>
        <tbody>
          <tr>
            <td colspan="6" style="text-align:center;">Nenhuma denúncia cadastrada.</td>
          </tr>
        </tbody>
      <?php endif; ?>           
    </table>
   </div>
</div>