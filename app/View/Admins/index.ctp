<div class="row">   
    
  <div class="span12 sessao">
    <h2> Administradores</h2>  
    <?php echo $this->Form->create('Search',array( 'url' => array('controller' => 'admins', 'action' => 'index')));?>  
    <div class="column-left">
      <fieldset>
        <p>
         <?php  echo $this->Form->input('name', array('label' => 'Nome do Administrador','required' => 'false', 'div' => false)); ?>                  
        </p>        
      </fieldset>
    </div>     
    <div class="column-left">
      <fieldset>
        <p>
            <?php  echo $this->Form->input('login', array('label' => 'Login','required' => 'false', 'div' => false)); ?>                             
        </p>   
      </fieldset>
    </div>
    <?php 
      $options = array('label' => 'Enviar', 'class' => 'btn  btn-small btn-success btn-search', 'div' => false);
      echo $this->Form->end($options);
    ?>                    
  </div>

  <div class="span12">
    <p>
      <a class="btn btn-primary btn-small"<?php echo $this->Html->link('Adicionar Administradores', array( 'controller' => 'admins', 'action' => 'add')) ?></a>
      </p> 
           
      <table class="table">
        <thead>
          <tr>
            <th>Nome do Usuário</th>
            <th>Login</th>
            <th>Tipo de Acesso</th>
            <th>Ações</th>
          </tr>
        </thead>
        
        <?php if(count($admins)):?>
          <tbody>  
            <?php foreach ($admins as $admin): ?>
              <tr>            
                <td><?php echo $admin['Admin']['name']?></td>
                <td><?php echo $admin['Admin']['username']?></td>
                <td><?php echo $admin['Admin']['role']?></td>
                <td>
                    <?php echo $this->Html->image('pencil.png',  
                                array('alt' => 'CakePHP',
                                      'url'=>  array ('action' => 'edit', $admin['Admin']['id']),
                                      array('escape' => false)))
                     ?>
                               
                    <?php echo $this->Form->postLink($this->Html->image('cross.png',array('alt' => _('CakePHP'))), 
                        array('action' => 'delete', $admin['Admin']['id']), 
                        array('escape' => false),
                       "Tem certeza que gostaria de deletar o Usuário?")             
                    ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
          <tfoot>
            <tr>
                <td colspan="4">
                  <div class="pagination">
                <?php 
                  echo $this->Paginator->prev('<<', null, null, array('class' => 'desabilitado'));
                  echo $this->Paginator->numbers();
                  echo $this->Paginator->next('>>', null, null, array('class' => 'desabilitado')); 
                ?>
              </div>
                  <div class="clear"></div>
              </td>
            </tr>
          </tfoot>
        <?php else: ?>
          <tbody>
            <tr>
              <td colspan="4" style="text-align:center;">Nenhum administrador cadastrado.</td>
            </tr>
          </tbody>
        <?php endif; ?>
                    
    </table>
   </div>
</div>