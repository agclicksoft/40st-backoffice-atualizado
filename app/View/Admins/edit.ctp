
<script type="text/javascript">
/*$(document).ready(function(){
   $("#AdminEditForm").submit(function(){
      var email = $("#AdminUsername").val();
      if(email = "")
      {
         alert('Digite um email!');
      }else      
         var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
         if(filtro.test(email))
         {
       alert("Este endereço de email é válido!");
       return true;
         } else {
           alert("Este endereço de email não é válido!");
           return false;
         }
      } else {
     return false;
      }
   });
});*/
</script>
<body>

<div class="admin form">
    <?php echo $this->Form->create('Admin',array('action' => 'edit'));?>
   
    <fieldset>
       
        <legend><?php echo __('Edição do Administrador'); ?></legend>

         <?php 
         echo $this->Form->input('name', array('label' => 'Nome do Administrador')); 
         echo $this->Form->input('username', array('label' => 'Login'));
         echo $this->Form->input('password', array('label' => 'Senha','value' =>'','required' => false));
         echo $this->Form->input('password_confirm', array('label' => 'Confirmar Senha','type' => 'password','required' => false));
         
         // echo $this->Form->input('role', array(
         //    'options' => array('admin' => 'Admin', 'author' => 'Author')
        //));
         echo $this->Form->input ('id',array('type' => 'hidden'));

         ?>

    </fieldset>
</div>
 <br>

<?php
$options = array
(
'label' => 'Salvar',

'class' => 'btn btn-small btn-success',
'div' =>  array('class' => 'control-group' )

);
 echo $this->Form->end($options);?>
 
<div class="control-group">
<?php echo $this->Html->link('Cancelar', '/admins/index', array( 'class' => 'btn btn-small btn-danger btn_danger_small', 'target' => '_Self')); ?>

</div>

</body>
</html>
 