<body>

<div class="admin form">
    <?php echo $this->Form->create('Admin', array( 'action' => 'add'));?>
   
    <fieldset>
       
        <legend><?php echo __('Adicionar Usuário'); ?></legend>

         <?php echo $this->Form->input('name', array('label' => 'Nome do Administrador')); 
         echo $this->Form->input('username', array('label' =>  'Login' ));
         echo $this->Form->input('password', array('label' =>  'Senha' ));
         echo $this->Form->input('password_confirm', array('label' => 'Confirmar Senha','type' => 'password',));
         // echo $this->Form->input('role', array(
         //    'options' => array('admin' => 'Admin', 'author' => 'Author')
        //));
         echo $this->Form->input('role', array('type' => 'hidden' , 'value' => 'admin'));
         ?>

    </fieldset>
</div>
 <br>

<?php
$options = array
(
'label' => 'Salvar',

'class' => 'btn btn-small btn-success',
'div' =>  array('class' => 'control-group' )

);
 echo $this->Form->end($options);?>
 
<div class="control-group">
<?php echo $this->Html->link('Cancelar', '/admins/index', array( 'class' => 'btn btn-small btn-danger btn_danger_small', 'target' => '_Self')); ?>

</div>

</body>
</html>