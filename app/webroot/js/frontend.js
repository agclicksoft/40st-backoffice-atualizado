jQuery(document).ready(function($){

	//$('#UserBirthday').mask('99/99/9999'); 	
  	       
  	$('#UserBirthday').on('keyup', function(){
  		mascara(this, mdata);
  	})
    $("#UserFirstName").change(function(){
    	var name = $('#UserFirstName').val();
	  	var nameValido = /^[a-zA-Z]{3,}$/i;
	      
	    if (!nameValido.test(name))
	    {
	        $("#error_name1").css('display','block');
	        $("#error_name1").css('color','red');  
	    }
	    else
	   	{
		  	$("#error_name1").css('display','none');		    
	   	}
    });

    $("#UserLastName").change(function(){
    	var lastname = $('#UserLastName').val();
	  	var lastnameValido = /^[a-zA-Z]{3,}$/i;
	      
	    if (!lastnameValido.test(lastname))
	    {	       
			$("#error_name2").css('display','block');
			$("#error_name2").css('color','red');
			//flag = 1; 
	    } 
	    else
	   	{
		  	$("#error_name2").css('display','none');		    
	   	}
    }); 	


    $("#UserEmail").change(function(){
    	var email = $('#UserEmail').val();
	    var emailValido=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if(!emailValido.test(email))
		{              	
	      	$("#error_email").css('display','block');
	        $("#error_email").css('color','red');
	    }
	    else
	   	{
		  	$("#error_email").css('display','none');		    
	   	} 
    });

    $("#UserPassword").change(function(){
    	var user_password = $('#UserPassword').val();
	  	var passwordValido = /^[a-zA-Z0-9]{8,}$/i;
	      
	    if (!passwordValido.test(user_password))
	    {  
	    	$("#error_password").css('display','block');
	        $("#error_password").css('color','red');
	    }
	    else
	   	{
		  	$("#error_password").css('display','none');		    
	   	}  
    });

    $("#UserPasswordConfirm").change(function(){
    	var confirm_user = $('#UserPassword').val();
	  	var confirm = $('#UserPasswordConfirm').val();	  	
	      
	    if (confirm != confirm_user)
	    {
	    	$("#error_verify").css('display','block');
	        $("#error_verify").css('color','red'); 
	    }
	    else
	   	{
		  	$("#error_verify").css('display','none');
		    
	   	}  
    });

    $("#UserBirthday").change(function(){
    	var birthday = $('#UserBirthday').val();
	 	var dataformat = birthday.split('/');
	 	var datausa = dataformat[1] +"/"+ dataformat[0]+"/"+dataformat[2];
	 	
	 	diff = diffDate(datausa);	         	
	   	if(diff >= 40)
	   	{
		    $("#error_birthday").css('display','none');
	   	}
	   	else
	   	{
		  	$("#error_birthday").css('display','block');
		    $("#error_birthday").css('color','red');
	   	} 
    })  	 
});

function enablesubmit()
{

    var flag = 0;

	var name = $('#UserFirstName').val();
  	var nameValido = /^[a-zA-Z]*$/i;
      
    if ($('#UserFirstName').val() == "")
    {
       	flag = 1; 
    }

 	var lastname = $('#UserLastName').val();
  	var lastnameValido = /^[a-zA-Z]*$/i;
      
    if ($('#UserLastName').val() == "")
    {
       	flag = 1; 
    } 

 	var email = $('#UserEmail').val();
    var emailValido=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	if(!emailValido.test(email))
	{              	
      	flag = 1; 
    } 
    
    if($('#UserPassword').length)
    {
	 	var user_password = $('#UserPassword').val();
	  	var passwordValido = /^[a-zA-Z0-9]{8,}$/i;
	      
	    if (!passwordValido.test(user_password))
	    {  
	    	flag = 1; 
	    } 

	 	var confirm_user = $('#UserPassword').val();
	  	var confirm = $('#UserPasswordConfirm').val();	  	
	      
	    if (confirm != confirm_user)
	    {
	    	flag = 1; 
	    } 
  	}
 	
 	var birthday = $('#UserBirthday').val();
 	var dataformat = birthday.split('/');
 	var datausa = dataformat[1] +"/"+ dataformat[0]+"/"+dataformat[2];
 	
 	diff = diffDate(datausa);	
 	if(!(diff >= 40))
   	{
	  	flag = 1;
   	}
   	
   	if($("#UserGender").val() == "")
   	{
   		flag = 1;
   	}

      
    if ($('#UserCountryId').val() == "")
    {
       	flag = 1; 
    }
   
    if ($('#UserStateId').val() == "")
    {
       flag = 1; 
    }

   /* if($("#UserAgree").val() == 0 || $("#UserAgree").val() == "")
   	{
   		flag = 1;
   	}*/

   	if (flag == 0)
   	{
   		$('#save_id').removeAttr('disabled');
      	$('#save_id').addClass('save_id_enable');   		
   	}
   	else
   	{
   		$('#save_id').attr('disabled', true);
      	$('#save_id').removeClass('save_id_enable');   		
   	}    	
}


function selectRadio(id)
{
  $(".radios").each(function(a){
    $(this).removeClass("radio-select");
  });

  $("#radio-" + id).addClass("radio-select");

  $("#UserGender").val(id);

  enablesubmit();
}

function selectCheck(id)
{
 
  if($("#check-" + id).hasClass("check-select") ==  true)
  {
    $("#check-" + id).removeClass("check-select");
     $("#UserAgree").val(0);
  }
  else
  {
    $("#check-" + id).addClass("check-select");
    $("#UserAgree").val(1);
  }

  enablesubmit();
}

function sendMessage()
{
	$("#pop").css('display','none');
	
    window.parent.postMessage("fechar", "*");
}

function sendMessageMobile()
{
	//alert('aqui');
	$("#pop").css('display','none');
	
    location.href = 'http://www.40street.com.br';
}