function diffDate(date)
{	
	dt = new Date(date);
	//alert("Data Hoje " + Date.now() + " data nascimento " + dt.getTime());
	var ageDifMs = Date.now() - dt.getTime();
	var ageDate = new Date(ageDifMs); // miliseconds from epoch
	
	return Math.abs(ageDate.getUTCFullYear() - 1970);	
}