<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
class Admin extends AppModel 
{
	//public $components = array('Security');
    public $uses = array('Admin');
    public $name = 'Admin';
	public $validate = array
    (
   	  'username' => array
	   (
            'usernameRule-1' => array(
                'rule' =>  '/^[a-z\.a-z]+$/' ,
                'message' => 'O login não pode ter espaço vazio,letras maiusculas e separação só por "."'
            ),
            'usernameRule-2' => array(
                'rule'    => 'isUnique',
                'message' => 'O login já existe'
            )
        ),
	   'password' => array
	   (
           'rule' => array('notEmpty'),
            'on' => 'create',
            'message' => 'Por favor inserir um password'
           
            
            
       ),
       'password_confirm' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'required' => true,
                'on' => 'create',
                'message' => 'Confirme sua senha.'
                
            ),
            'minLength' => array(
                'rule' => array('minLength', 6),
                'required' => true,
                 'on' => 'create',
                'message' => 'Sua senha precisa conter pelo menos 6 caracteres.'
            ),
            'passwordConfirmation' => array(
                'rule'    => array('passwordConfirmation'),                 
                'message' => 'As duas senhas não conferem.'                                           )   
           )                                              
                            
    );

    public function beforeSave($options = array()) 
    {
        if(empty($this->data[$this->alias]['password']))
      {
        unset($this->data[$this->alias]['password']);
       }

       if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
    
    return true;

    /*    if (isset($this->data[$this->alias]['password'])) 
        {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }

        return true;*/
    }        
    
    public function passwordConfirmation($data)
    {

        $password = $this->data['Admin']['password'];
        $password_confirmation = $this->data['Admin']['password_confirm'];
          
        if($password===$password_confirmation)
        {
                 
            return true;
                 
        }
        else
        {

            return false;
                 
        }
 
    }

}
?>