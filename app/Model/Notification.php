<?php
App::uses('AppModel', 'Model');
App::uses('User', 'Model');
App::uses('DeviceInfos', 'Model');
class Notification extends AppModel
{
	public $name = 'Notification';

	public function sendPushChat($notification_id, $body)
	{
        //echo "<pre>";
        //$b = str_replace(array("\\"),"",$body); 
        //echo $b;
        $body = json_decode($body);
        //exit;
		$array = array();
		
		$count = $this->find('all', 
			array("conditions" => 
				array("Notification.notified" => $body->receipt_id, 
                      "Notification.notifier" => $body->user_id, 
					  "type" => "chat", 
                      "status" => array("not_send")
				)
			)
		);

		$array['aps'] = array(
			'alert' => array('body' => $body->message, 'action-loc-key' => 'Conferir'),
			'sound' => 'default',
			'badge' => sizeof($count),
			'user_id' => $body->user_id,
			'receipt_id' => $body->receipt_id,
			'match_id' => $body->match_id,
			'id' => $body->id,
			'created' => $body->created,
			'type' => 'chat',
			'message' => $body->msg
        );

        $userQuery = new User();

        $user = $userQuery->find("first", 
        	//array("conditions" => array("User.id" => $body->user_id))
            array("conditions" => array("User.id" => $body->receipt_id))
        );

        if (count($user))
        {
        	$array['devices'] = array( 'google' => array(), 'apple' => array() );
            //$devices_ids = array( 'google' => array(), 'apple' => array() );

        	foreach($user["DeviceInfos"] as $key => $device)
        	{ 
                /*echo "<pre>";
                print_r($device);*/
        		//$array["devices"][] = array("token" => $device['device_token']);

                if($device['device_type'] == 'Android'){
                    $array["devices"]['google'][] = $device['device_token'];
                } else {
                    $array["devices"]['apple'][] = $device['device_token'];
                }
        	}
        }
        //echo "aqui 2";
        if($this->sendPush($array))
        {
            //echo "aqui";
           	$conditions = array("Notification.notified" => $body->receipt_id, "Notification.notifier" => $body->user_id, "type" => "chat", "status" => "not_send");
			$this->updateAll(array("Notification.status" => "'send'"), $conditions);
        }
        
    }


	/*private function sendPush($array)
	{
		$passphrase = 'push40st';

        $ctx = stream_context_create();

        stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/Controller/ck.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        $body['aps'] = $array['aps'];

        $payload = json_encode($body);

        foreach($array['devices'] as $keyT => $device)
        {
            $deviceToken = $device['token'];

            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$fp)
                exit("Falha na conexao: $err $errstr" . PHP_EOL);

            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

            $result = fwrite($fp, $msg, strlen($msg));

            fclose($fp);    

            if (!$result)
                return false;
            else
                return true;
        } 
	}*/
    private function sendPush($array)
    {

        /*echo "<pre>";
        print_r($array);
        exit;*/

        $aps = $array['aps'];
        $alerts = $aps['alert'];
        $msg = $alerts['body']." : ". $aps["message"];

        if(count($array['devices']['google']) > 0){          
            
            $devices = $array['devices']['google'];
            return $this->sendPushGoogle($aps, $alerts, $msg, $devices);
        }
        else{

            $devices = $array['devices']['apple'];
            return $this->sendPushApple($aps, $alerts, $msg,$devices);
        }      

    }

    private function sendPushGoogle($aps, $alerts, $msg, $devices)
    {

        define("GOOGLE_API_KEY", "AIzaSyAONKuTPN78R4iKi5iLpeGPZ2vgZcgx5-0");
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $message = array(
            "id" => $aps['id'],
            "title" => "40street",
            "message" => $msg,
            "type" => $aps['type'],
            "user_id" => $aps['user_id'],
            "receipt_id" => $aps['receipt_id'],
            "created" => $aps['created'],
            "msg" => $aps['message'],
            "match_id" => $aps['match_id']
        );

        $fields = array(
            'registration_ids' => $devices,
            'data' => $message,
        );

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        echo $result;
        if (!$result)
                return false;
            else
                return true; 
    }

    private function sendPushApple($aps, $alerts, $msg, $devices)
    {
        $passphrase = 'push40st';

        $ctx = stream_context_create();

        stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/Controller/ck.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        $body['aps'] = $array['aps'];

        $payload = json_encode($body);

        foreach($array['devices'] as $keyT => $device)
        {
            $deviceToken = $device['token'];

            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$fp)
                exit("Falha na conexao: $err $errstr" . PHP_EOL);

            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

            $result = fwrite($fp, $msg, strlen($msg));

            fclose($fp);    

            if (!$result)
                return false;
            else
                return true;
        } 
    }
}
?>