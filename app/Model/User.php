<?php
    App::uses('AppModel', 'Model');
    App::uses('AuthComponent', 'Controller/Component');
	class User extends AppModel{
		public $belongsTo = array ( 'Country', 'State');
		public $hasMany = array(
								'UserAction', 'UserInteration', 'DeviceInfos',
								'SenderUser' => array('className' => 'Talk', 'foreignKey' => 'user_id'),
								'RecipientUser' => array('className' => 'Talk', 'foreignKey' => 'receipt_id')
						);
		//public $belongsTo = 'State';
		public $name = 'User';
		public $validate = array(

           /* 'first_name' =>  array(
           	    'rule' => '/^[a-zA-Z]{3,}$/i',
           	    'required' => true,
           	    'allowEmpty' => false,
           	    'message' => 'O campo é Obrigatório, só letras permitidas e com o minímo de 3'


           ),
           'last_name' =>  array(
           	    'rule' => '/^[a-zA-Z]{3,}$/i',
           	    'required' => true,
           	    'allowEmpty' => false,
           	    'message' => 'O campo é Obrigatório, só letras permitidas e com o minímo de 3'


           ),

           'email' => array(
                      'rule' => array('email',true),
                      'message' => 'O email não está num formato valido'
           ),
           
           'password' => array(
		        'rule' => array('minLength', 8),
		        'allowEmpty' => false,
		        'message' => 'A senha deve ter no minímo 8 caracteres'
		    ),
		    
		    'password_confirm' => array(
	            'notEmpty' => array(
	                'rule' => 'notEmpty',
	                'required' => true,
	                'message' => 'Por favor confirme sua senha.'
	            ),    
			  
				'minLength' => array(
	                'rule' => array('notEmpty', 'minLength', 8),
	                'required' => true,
	                'message' => 'Sua confirmação da senha precisa conter pelo menos 8 caracteres.'
	            ),
	            'passwordConfirmation' => array(
                'rule'    => array('passwordConfirmation'),                 
                'message' => 'As duas senhas não conferem.'
                ),
            ),

			'birthday' => array(
			  'age' => array(
			    'rule' => 'checkOver40',
			    'message' => 'Para registra-se tem que ter 40 anos ou mais'
			  ),
			),

            'gender' => array(
            'rule' => 'notEmpty',
            'allowEmpty' => false,
            'message' => 'Por favor escolher uma das opções'
            ),

            'country' => array(
            'rule' => 'notEmpty',
            'allowEmpty' => false,
            'message' => 'Por favor escolher uma das opções'
            ),

            'state' => array(
            'rule' => 'notEmpty',
            'allowEmpty' => false,
            'message' => 'Por favor escolher uma das opções'
            )*/
        
        );
        public function beforeSave($options = array()) 
	    {	    	
	        if(empty($this->data[$this->alias]['password']))
	      	{
	        	unset($this->data[$this->alias]['password']);
	       	}

	       	if (isset($this->data[$this->alias]['password'])) 
	       	{
	            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
	        }
	    
	    	$this->data[$this->alias]['salt'] = md5(uniqid(mt_rand(), true));
	    	
	    	return true;
        } 

		public function passwordConfirmation($data)
	    {

	        $password = $this->data['User']['password'];
	        $password_confirmation = $this->data['User']['password_confirm'];
	          
	        if($password===$password_confirmation)
	        {
	                 
	            return true;
	                 
	        }
	        else
	        {

	            return false;
	                 
	        }
	 
	    }
		
		public function checkOver40($check) 
		{
			$bday = strtotime($check['birthday']);
			if (time() < strtotime('+40 years', $bday)) return false;
				return true;
		}
  
    }
?>