<?php

App::uses('AppModel', 'Model');
App::uses('Notification', 'Model');
App::uses('User', 'Model');
class Talk extends AppModel
{
	public $name = 'Talk';
	public $belongsTo = array(
		'SenderTalk' => array('className' => 'User', 'foreignKey' => 'user_id'),
		'RecipientTalk' => array('className' => 'User', 'foreignKey' => 'receipt_id'),
		'Match' => array('className' => 'Match', 'foreignKey' => 'match_id')
	);

	public function afterSave($created, $options = array())
	{
		if($created)
		{
			$notification = new Notification();

			$data = array();

			$data['Notification'] = array();

			$data['Notification']["notified"] = $this->data[$this->alias]['receipt_id'];
			$data['Notification']["notifier"] = $this->data[$this->alias]['user_id'];
			
			$userQuery = new User();

			$user = $userQuery->find("first", 
				array("conditions" => array("User.id" => $this->data[$this->alias]['user_id']))
			);
			
			$message = "O " . $user["User"]["name"] . " enviou uma mensagem";
			$json = array("message" => $message,
						  "status" => $this->data[$this->alias]["status"],
						  "match_id" => $this->data[$this->alias]["match_id"],
						  "receipt_id" => $this->data[$this->alias]["receipt_id"],
						  "user_id" => $this->data[$this->alias]["user_id"],
						  "id" => $this->data[$this->alias]["id"],
						  "created" => $this->data[$this->alias]["created"],
						  "msg" => $this->data[$this->alias]["message"]);

			$data["Notification"]["body"] = json_encode($json);

			$data["Notification"]["status"] = "not_send";

			$data["Notification"]["type"] = "chat";

			$notification->save($data);


		}
	}


	public function changeStatus($chat, $id = null)
	{	
		if(isset($chat['Talk']['status']) || isset($id))
		{
			
			$condTalkId = array('Talk.id' => $id);
			$thereIsTalkId = $this->hasAny($condTalkId);
			if($thereIsTalkId)
			{
				$talk = $this->findById($id);

				switch ($chat['Talk']['status'])
				{
					case 'delivered':
						
						if($talk['Talk']['status'] == 'save_sever')
						{
							$this->id = $talk['Talk']['id'];
							$this->set('status','delivered');
							$this->save();
							return array('status' => '200', 'talk_id' => $id);
						}
						else if($talk['Talk']['status'] == 'delivered')
						{
							return array('status' => '200', 'talk_id' => $id);
						}
						
						break;
					case 'read':
						
						if($talk['Talk']['status'] == 'delivered' || $talk['Talk']['status'] == 'save_sever')
						{
							$this->id = $talk['Talk']['id'];
							$this->set('status','read');
							$this->save();
							return array('status' => '200', 'talk_id' => $id);
						}
						else if($talk['Talk']['status'] == 'read')
						{
							return array('status' => '200', 'talk_id' => $id);
						}

						break;				
				}
			}

			return array('status' => '500', 'message' => 'Problema na alteracao do status');
		}

		return array('status' => '500', 'message' => 'informe um id e um talk');
	}
	
}

?>