<?php
	class UserAction extends AppModel{
		public $name = 'UserAction';
		public $belongsTo = array('User', 'Match');

		public function beforeSave($options = array())
		{
			if(!$this->id && !isset($this->data[$this->alias][$this->primaryKey]))
			{
				$conditions = array("UserAction.user_id" => $this->data[$this->alias]["user_id"], "UserAction.match_id" => $this->data[$this->alias]["match_id"], "UserAction.action" => $this->data[$this->alias]["action"]);
				$this->updateAll(array("UserAction.active" => 'false'), $conditions);
				$this->data[$this->alias]["active"] = true;
			}
		}
	}
?>