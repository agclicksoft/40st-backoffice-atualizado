<?php
App::uses('FrontendsController', 'Controller');
class UsersController extends FrontendsController
{
	public $layout = 'mobile';
	public $helpers = array('Html','Form','Js' => array('Jquery'));
	public $name = 'Users';
	public $uses = array('UserInteration', 'User'); 
	public $components = array('Session');

	public function beforeFilter() 
	{
		parent::beforeFilter();
	}
	
	public function add()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;				

			$conditions = array('User.facebook_id' => $user["User"]["facebook_id"]);

			if ($this->User->hasAny($conditions))
			{
				$user = $this->User->find('first', array('conditions' => $conditions));
				$conditionI = array('UserImage.user_id' => $user["User"]["id"]);
				$this->loadModel('UserImage');
				$userImage = $this->UserImage->find('all', array('conditions' => $conditionI));
				$array_images = array();
				$cI = 0;

				foreach($userImage as $keyI => $image)
				{
					$array_images[] = array("id" => $image["UserImage"]["id"] , "src" => Configure::read("SITE_PATH") . $image["UserImage"]["dir"] . $image["UserImage"]["filename"] );									 	
					$cI ++;
				}

				$count = $cI;

				$this->loadModel('DeviceInfos');

				$conditions = array(
					'DeviceInfos.user_id' => $user["User"]["id"],
					'DeviceInfos.device_id' => $user["User"]["device_id"]
					);

				if($this->DeviceInfos->hasAny($conditions))
				{						
					$device = $this->DeviceInfos->find('first', array('conditions' => $conditions));

					$this->DeviceInfos->read(null, $device["DeviceInfos"]["id"]);

					$this->DeviceInfos->set(array(
						'device_token' => $user['User']['device_token']
						));

					$this->DeviceInfos->save();
				}
				else
				{
					$this->DeviceInfos->set(array(
						'user_id' => $user['User']['id'],
						'device_id' => $user['User']['device_id'],
						'device_token' => $user['User']['device_token'],
						'device_type' => $user['User']['device_type']
						));

					$this->DeviceInfos->save();
				}
				

				$json = array('status' => 200, 'user' => $user["User"], "images" => $array_images ,"count" => $count );
			}
			else if($this->User->save($user))
			{
				App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
				$facebook = new Facebook(array(
					'appId'  => Configure::read("FB_APP_ID"),
					'secret' => Configure::read("FB_APP_SECRET"),
					));

				$facebook->setAccessToken($user["User"]['facebook_token']);
				$user_profile = $facebook->api('/me');
				$user_info = $facebook->api('/me?fields=gender');
				$user = $this->User->read(null, $this->User->id);

				if($user_info["gender"] == "female")
				{
					$gender = "2";
				}
				else if($user_info["gender"] == "male")
				{
					$gender = "1";
				}

				$user_albums = $facebook->api('/me/albums');
				$dir = 'app/webroot/image/';
				$array_images = array();
				$cI = 0;

				foreach($user_albums['data'] as $keyA => $album )
				{
					if($album['name'] == "Profile Pictures")
					{
						$api_album = "/" . $album['id'] . "/photos";
						$user_photos = $facebook->api($api_album);

						for($keyF = 0; $keyF < 4; $keyF++)
						{
							if(isset($user_photos['data'][$keyF]))
							{
								$photo = $user_photos['data'][$keyF];

								$filename = $album['from']['id'] . '_' . $keyF . '.jpg';

								$url = $user_photos['data'][$keyF]['source'];
								$img = $dir . $filename;
								file_put_contents($img, file_get_contents($url));

								$this->loadModel('UserImage');
								$this->UserImage->set(array('id' => null,
									'filename' => $filename,
									'dir' => $dir,
									'mimetype' => null,
									'filesize' => filesize($img),
									'position' => $keyF,
									'user_id' => $user["User"]['id']));
								
								$this->UserImage->save();
								$image = $this->UserImage->read();
								$array_images[] = array("id" => $image["UserImage"]["id"] , "src" => Configure::read("SITE_PATH") . $image["UserImage"]["dir"] . $image["UserImage"]["filename"] );									 	
								$cI ++;
							}		 				
						}
					}
				}
				
				$count = $cI;

				$this->loadModel('DeviceInfos');

				$this->DeviceInfos->set(array(
					'user_id' => $user['User']['id'],
					'device_id' => $user['User']['device_id'],
					'device_token' => $user['User']['device_token'],
					'device_type' => $user['User']['device_type']
					));

				$this->DeviceInfos->save();

				$this->User->set(array(
					'gender' => $gender,
					'quant_photo' => $count
					));

				$this->User->save();

				$json = array('status' => 200, 'user' => $user["User"], "images" => $array_images, "count" => $count);
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}
		
		echo json_encode($json);
	}

	public function edit()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			if($this->User->hasAny(array('User.id' => $user['User']['id'])))
			{
				$this->User->id = $user['User']['id'];

				$this->User->set(array(
					'description' => $user["User"]["description"]
					));

				if($this->User->save())
				{
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}			
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function edituser()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			if($this->User->hasAny(array('User.id' => $user['User']['id'])))
			{
				$this->User->id = $user['User']['id'];

				$this->User->set(array(
					'description' => $user["User"]["description"],
					'visible' => $user["User"]["visible"],
					'interest' => $user["User"]["interest"],
					'distance' => $user["User"]["distance"],
					'min_age' => $user["User"]["min_age"],
					'max_age' => $user["User"]["max_age"],
					'show_newcomb' => $user["User"]["show_newcomb"],
					'show_msgs' => $user["User"]["show_msgs"],
					'show_age' => $user["User"]["show_age"],
					'agree' => $user["User"]["agree"]
					));

				if($this->User->save())
				{
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500, 'error' => print_r($this->User->validationErrors, true));
				}
			}
			else
			{
				$json = array('status' => 501, 'error' => 'Nenhum usuario encontrado');
			}
		}
		else
		{
			$json = array('status' => 502, 'error' => 'Conexao sem post');
		}

		echo json_encode($json);
	}

	public function updatecountphoto()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			if($this->User->hasAny(array('User.id' => $user['User']['id'])))
			{
				$this->User->id = $user['User']['id'];

				$this->User->set(array(
					'quant_photo' => $user["User"]["count"]
					));

				if($this->User->save())
				{
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function albums()
	{
		App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
		$facebook = new Facebook(array(
			'appId'  => Configure::read("FB_APP_ID"),
			'secret' => Configure::read("FB_APP_SECRET"),
			));

		if($this->request->is('post'))
		{
			$user = $this->request->data;

			$conditions = array('User.facebook_id' => $user["User"]['id']);

			if ($this->User->hasAny($conditions))
			{
				$user = $this->User->find('first', array('conditions' => $conditions));
				$facebook->setAccessToken($user['User']["facebook_token"]);
				$user_albums = $facebook->api('/me/albums');

				$albums = array();

				foreach($user_albums['data'] as $keyA => $album)
				{
					$api_album = "/" . $album['id'] . "/photos";
					$photos_album = $facebook->api($api_album);
					$photos = array();

					foreach($photos_album['data'] as $keyP => $photo)
					{
						$photos[] = array('id' => $photo['id'], 'source' => $photo['source']);
					}

					$albums[] = array('id' => $album['id'], 'name' => $album['name'], 'picture' => $photos_album['data'][0]['picture'], 'photos' => $photos);
				}

				$json = array('status' => 200, 'albums' => $albums);
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function uploadphoto()
	{
		if($this->request->is('post'))
		{
			$dir = 'app/webroot/image/';
			$photo = $this->request->data;

			$this->loadModel('User');
			$this->loadModel('UserImage');					

			$conditions = array('User.facebook_id' => $photo['UserImage']['user']);
			$user = $this->User->find('first', array('conditions' => $conditions));

			$conditions = array('UserImage.user_id' => $user['User']['id'], 'UserImage.position' => $photo['UserImage']['position']);
			$image = $this->UserImage->find('first', array('conditions' => $conditions));

			if(isset($_FILES["file"]["tmp_name"]))
			{
				$filename = $_POST["user"] . '_' . $_POST['position'] . '.jpg';

				$pathimage = $dir . $filename;

				move_uploaded_file($_FILES["file"]["tmp_name"], $pathimage);
			}
			else
			{
				$filename = $photo['UserImage']['user'] . '_' . $photo['UserImage']['position'] . '.jpg';
				$url = str_replace(",","&",$photo['UserImage']['url']);

				$img = $dir . $filename;
				file_put_contents($img, file_get_contents($url));
			}

			if($image == null)
			{
				$this->UserImage->set(array(
					'filename' => $filename,
					'dir' => $dir,
					'mimetype' => null,
					'filesize' => filesize($img),
					'position' => $photo['UserImage']['position'],
					'user_id' => $user["User"]['id']
					));
				
				$this->UserImage->save();
			}
			
			$json = array('status' => 200);
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function delphoto()
	{
		if($this->request->is('post'))
		{
			$photo = $this->request->data;

			$dir = 'app/webroot/image/';
			$id = $photo['UserImage']['user'];
			$pos = $photo['UserImage']['position'];

			$filename = $id . "_" . $pos . ".jpg";
			$file = $dir . $filename;

			if(file_exists($file))
			{
				$this->loadModel('UserImage');

				$conditions = array('UserImage.filename' => $filename);
				$getItem = $this->UserImage->find('first', array('conditions' => $conditions));
				$id = $getItem['UserImage']['id'];

				if($this->UserImage->delete($id))
				{
					unlink($file);
					
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 300);
			}
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function searchuser()
	{
		if($this->request->is('post'))
		{
			// pega solicitacao
			$user = $this->request->data;
			// valida solicitacao
			if(!isset($user["User"]['id'])){
				$json = array('status' => 500, 'result' => "user id NOT pass");
				echo json_encode($json);
				return;
			}
			// chama funcoes para utilizar app do facebook
			App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
			$facebook = new Facebook(array(
				'appId'  => Configure::read("FB_APP_ID"),
				'secret' => Configure::read("FB_APP_SECRET"),
				));
			// ******************************************* verifica e corrige o limite caso passado errado
			if(!isset($user['User']['limit'])){
				$limit = 3;
			} else {
				$limit = $user['User']['limit'];
				if(!is_numeric($limit) AND $limit > 5){
					$limit = 3;
				}
			}
			// ******************************************* offset
			if(!isset($user['User']['offset'])){
				$offset = 0;
			} else {
				$offset = $user['User']['offset'];
				if(!is_numeric($offset)){
					$offset = 0;
				}
			}
			// ******************************************* gender
			// default value. nao filtra
			$gender = null;
			if(!isset($user['User']['gender'])){
				/*$json = array('status' => 500, 'result' => "gender NOT pass");
				echo json_encode($json);
				return;*/
			} else {
				$gender_tmp = $user['User']['gender'];
				if(!is_numeric($gender)){
					/*$json = array('status' => 500, 'result' => "gender NOT numeric: $gender");
					echo json_encode($json);
					return;*/
				} else {
					$gender_tmp = (int) $gender_tmp;
					if(!in_array($gender_tmp, array(1, 2))){
						/*$json = array('status' => 500, 'result' => "gender Invalid: $gender");
						echo json_encode($json);
						return;*/
					} else {
						// monta sql com gender
						$gender = '('.$gender_tmp.',3)';
						$gender = "User.interest IN " . $gender;
					}
				}
			}
			// ******************************************* interest
			// default value. nao filtra
			$interest = null;
			if(!isset($user['User']['interest'])){
				$json = array('status' => 500, 'result' => "interest NOT pass");
				echo json_encode($json);
				return;
			} else {
				$interest_tmp = $user['User']['interest'];
				if(!is_numeric($interest_tmp)){
					$json = array('status' => 500, 'result' => "interest NOT numeric: $interest");
					echo json_encode($json);
					return;
				} else {
					$interest_tmp = (int) $interest_tmp;
					if(!in_array($interest_tmp, array(1, 2, 3))){
						$json = array('status' => 500, 'result' => "interest Invalid: $gender");
						echo json_encode($json);
						return;
					} else {
						// monta sql com interest
						if($interest_tmp == '3')
						{
							$interest = "('1', '2')";
						} else {
							$interest = "('$interest_tmp')";
						}
						$interest = "User.gender IN " . $interest;
					}
				}
			}
			// ******************************************* pos_lat + pos_long
			if(!isset($user["User"]["pos_lat"]) OR !isset($user["User"]["pos_long"])){
				$latuser = 0;
				$longuser = 0;
			} else {
				$latuser = $user["User"]["pos_lat"];
				$longuser = $user["User"]["pos_long"];
			}
			// verifica se vazio
			$latuser = (empty($latuser) == true) ? 'null' : $latuser;
			$longuser = (empty($longuser) == true) ? 'null' : $longuser;
			// corrige erro de tipo no mysql
			$latuser = "CAST('".$latuser."' AS SIGNED)";
			$longuser = "CAST('".$longuser."' AS SIGNED)";
			// verifica se chamada de perfil
			if(isset($perfil)){
				$result = $this->User->find('all',
					array(
						'fields' => array("*", "DISTANCE(" . $latuser . ", " . $longuser . ", pos_lat, pos_long) AS diffdistance"),
						'conditions' => array("User.id =" => $offset),
						'limit' => $limit)
					);
			} else {
				// ******************************************* verifica se o offset esta acima do limite da tabela e
				$offset = (int)$offset;
				$offset_where = '';
				$result = $this->User->find('all', array(
					'fields' => array("MAX(User.id) AS IDmax"), 
					'conditions' => array(), ));
				if(isset($result[0][0]['IDmax'])){
					$IDmax = (int)$result[0][0]['IDmax'];
					if(is_integer($IDmax)){
						if($IDmax > $offset){
							$offset_where = "User.id > ".$offset;
						}
					}
				}
				// ******************************************* faz a primeira busca com offset E nao votados
				$result = $this->User->find('all', array(
					'fields' => array("*", "DISTANCE(" . $latuser . ", " . $longuser . ", pos_lat, pos_long) AS diffdistance"), 
					'conditions' => array(
						'User.visible' => '1',
						'User.id !=' => $user["User"]['id'],
						"`User`.`facebook_id` IS NOT NULL AND `User`.`facebook_id` > '0' AND `User`.`facebook_id` != 'undefined'",
					$offset_where, //'User.id >' => $offset,
					"TIMESTAMPDIFF(YEAR,birthday,curdate()) BETWEEN ? AND ?" => array($user["User"]["min_age"]-1, $user["User"]["max_age"]+1),
					$interest,
					$gender,
					"NOT EXISTS (SELECT id FROM relations WHERE relater_id = " . $user['User']['id'] . " AND related_id = User.id)"
					), 'limit' => $limit)
				);
				// ******************************************* segunda busca sem offset E nao votados
				if(!count($result)){
					$result = $this->User->find('all', array(
						'fields' => array("*", "DISTANCE(" . $latuser . ", " . $longuser . ", pos_lat, pos_long) AS diffdistance"), 
						'conditions' => array(
							'User.visible' => '1',
							'User.id !=' => $user["User"]['id'],
							"`User`.`facebook_id` IS NOT NULL AND `User`.`facebook_id` > '0' AND `User`.`facebook_id` != 'undefined'",
							"TIMESTAMPDIFF(YEAR,birthday,curdate()) BETWEEN ? AND ?" => array($user["User"]["min_age"]-1, $user["User"]["max_age"]+1),
							$interest,
							$gender,
							"NOT EXISTS (SELECT id FROM relations WHERE relater_id = " . $user['User']['id'] . " AND related_id = User.id)"
							), 'limit' => $limit));
				}
				// ******************************************* segunda busca sem offset e votados SOMENTE COMO DUVIDA
				if(!count($result)){
					$result = $this->User->find('all', array(
						'fields' => array("*", "DISTANCE(" . $latuser . ", " . $longuser . ", pos_lat, pos_long) AS diffdistance"), 
						'conditions' => array(
							'User.visible' => '1',
							'User.id !=' => $user["User"]['id'],
							"`User`.`facebook_id` IS NOT NULL AND `User`.`facebook_id` > '0' AND `User`.`facebook_id` != 'undefined'",
							"TIMESTAMPDIFF(YEAR,birthday,curdate()) BETWEEN ? AND ?" => array($user["User"]["min_age"]-1, $user["User"]["max_age"]+1),
							$interest,
							$gender,
							"NOT EXISTS (SELECT id FROM relations WHERE relater_id = " . $user['User']['id'] . " AND related_id = User.id and opt <> 2)"
							), 'limit' => $limit));
				}
				// informa que nao achou ninguem
				if(!count($result)){
					$json = array('status' => 200, 'result' => array() );
					echo json_encode($json);
					return;
				}
				// ???
				$this->loadModel('UserImages');
				$this->loadModel('Relation');
				// ******************************************* tentativa de reducao no tempo de resposta do search E limitecao de informacoes privadas sendo enviadas indiscriminadamente
				$users_list = array();
				foreach($result as $keyI => $users)
				{
					$photos = $this->UserImages->find('all', array('fields' => array('position'),
						'conditions' => array('UserImages.user_id' => $users['User']['id']),
						'limit' => 4));

					$match = $this->Relation->hasAny(array('Relation.relater_id' => $users['User']['id'],
						'Relation.related_id' => $user["User"]['id'],
						'Relation.opt' => 1));
					try
					{
						$commons = $facebook->api('/' . $users['User']['facebook_id'] . '?fields=context.fields(mutual_likes),context.fields(mutual_friends)');		
						$result[$keyI]['User']['commons'] = $commons;
					}	
					catch(Exception $e)
					{
						$result[$keyI]['User']['commons'] = array();		        		
					}
					$result[$keyI]['User']['photos'] = $photos;
					$result[$keyI]['User']['match'] = $match;
					// teste de correcao para seguranca do usuario
					unset($result[$keyI]['User']['salt']);
					unset($result[$keyI]['User']['password']);
					unset($result[$keyI]['User']['email']);
					unset($result[$keyI]['User']['pos_long']);
					unset($result[$keyI]['User']['pos_lat']);
					// teste de reducao de na rsposta do serach
					$users_list[$keyI]['User'] = $result[$keyI]['User'];
					$users_list[$keyI]['0'] = $result[$keyI]['0'];
				}

				$json = array('status' => 200, 'result' => $users_list);
			}
		} else {
			$json = array('status' => 500);
		}
		echo json_encode($json);
	}

	public function updategeoposition()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			$latitude = $user['User']['pos_lat'];
			$longitude = $user["User"]['pos_long'];

			if($this->User->hasAny(array('User.id' => $user['User']['id'])))
			{
				$this->User->id = $user['User']['id'];

				$this->User->set(array(	
					'pos_lat' => $latitude,
					'pos_long' => $longitude
					)
				);
				
				if($this->User->save())
				{
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}
		
		echo json_encode($json);
	}

	public function deleteuser()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			if($this->User->hasAny(array('User.id' => $user['User']['id'])))
			{
				$this->User->id = $user['User']['id'];

				$this->loadModel('UserImages');

				$images = $this->UserImages->find('all', array('conditions' => array('user_id' => $user['User']['id']))); 

				foreach($images as $keyI => $image)
				{
					$filename = $image['UserImages']['dir'] . $image['UserImages']['filename'];

					if(file_exists($filename))
					{
						unlink($filename);		
					}
				}

				if($this->User->delete())
				{
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}
		
		echo json_encode($json);			
	}

	

	public function returnoptreport()
	{
		$this->loadModel('Optreport');
		
		$json = array('status' => 200, 'result' => $this->Optreport->find('all'));

		echo json_encode($json);
	}

	public function toreport()
	{
		if($this->request->is('post'))
		{
			$report = $this->request->data;

			$this->loadModel('Report');

			$this->Report->set(array(
				'reported_id' => $report["Report"]["reported_id"],
				'reporter_id' => $report["Report"]["reporter_id"],
				'opt_report' => $report["Report"]["opt_report"],
				'description' => $report["Report"]["description"]
				));

			if($this->Report->save())
			{
				$json = array('status' => 200, 'id' => $report["Report"]["id"]);
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function setrelation()
	{
		if($this->request->is('post'))
		{
			$this->loadModel('Relation');
			$relation = $this->request->data;
			/*$relation["Relation"]["searcher_id"] = 924;
			$relation["Relation"]["searched_id"] = 1000;
			$relation["Relation"]["opt_relation"] = 1;*/

			if(empty($relation["Relation"]["searcher_id"]) OR
				empty($relation["Relation"]["searcher_id"]) OR
				!in_array($relation["Relation"]["opt_relation"], array('1','2','3')) ){

				$json = array('status' => 500);

			} else {
				$condRelation = array('Relation.relater_id' => $relation["Relation"]["searcher_id"], 
					'Relation.related_id' => $relation["Relation"]["searched_id"]);

				$rel = $this->Relation->find('first', array('conditions' => $condRelation));

				if(empty($rel)){
					try{
						$this->Relation->set(array(
							'relater_id' => $relation["Relation"]["searcher_id"],
							'related_id' => $relation["Relation"]["searched_id"],
							'opt' => $relation["Relation"]["opt_relation"]
							));

						if($this->Relation->save())
						{
							$json = array('status' => 200, 
								'id' => $this->Relation->id, 
								'matched' => $relation["Relation"]["searched_id"], 
								'opt' => $relation["Relation"]["opt_relation"]
								);
						}
						else
						{
							$json = array('status' => 500);
						}
					} catch (Exception $e) {
						$json = array('status' => 500);
					}
				} else {
					$this->Relation->id = $rel["Relation"]["id"];
					$this->Relation->set('opt',$relation["Relation"]["opt_relation"]);

					if($this->Relation->save())
					{
						$json = array('status' => 200, 
							'id' => $this->Relation->id, 
							'matched' => $relation["Relation"]["searched_id"], 
							'opt' => $relation["Relation"]["opt_relation"]
							);
					}
					else
					{
						$json = array('status' => 500);
					}
				}
			}
		} else {
			$json = array('status' => 500);
		}
		echo json_encode($json);
	}

	public function creatematchnotification()
	{
		if($this->request->is('post'))
		{
			$this->loadModel('Match');
			$this->loadModel('Notification');
			$notification = $this->request->data;
/*$notification["Notification"]["notifier"] = 1000;
$notification["Notification"]["notified"] = 880;*/

			if(empty($notification["Notification"]["notifier"]) OR empty($notification["Notification"]["notified"])){
				$json = array('status' => 500);

			} else {
				$condRelation = array('Match.matcher' => $notification["Notification"]["notifier"],
					'Match.matched' => $notification["Notification"]["notified"]);

				$rel = $this->Match->find('first', array('conditions' => $condRelation));

				if(!empty($rel)){
					$this->Match->id = $rel["Match"]["id"];
				}
				$this->Match->set(array(
					'matcher' => $notification['Notification']['notifier'],
					'matched' => $notification['Notification']['notified']
				));
				
				try {
					if($this->Match->save())
					{
						$this->Notification->set(array(
							'notifier' => $notification['Notification']['notifier'],
							'notified' => $notification['Notification']['notified'],
							'body' => 'também curtiu você.'
							));

						if($this->Notification->save())
						{
							$json = array('status' => 200);
						}
						else
						{
							$json = array('status' => 500);
						}
					}
					else
					{
						$json = array('status' => 500);
					}
				} catch (Exception $e) {
					$json = array('status' => 500);
				}
			}				
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function sendmatchnotification()
	{
		$this->loadModel('User');
		$this->loadModel('Notification');
		$this->loadModel('DeviceInfos');
		$this->loadModel('UserImages');

		$pendents = $this->Notification->find('all', array('limit' => 10, "conditions" => array("Notification.status" => null)));

		foreach ($pendents as $keyP => $notification)
		{
			$id_notified = $notification['Notification']['notified'];
			$id_notifier = $notification['Notification']['notifier'];

			
			$tokens = $this->DeviceInfos->find('all', array(
				'fields' => array('device_token'),
				'conditions' => array('DeviceInfos.user_id' => $id_notified)
				));

			$this->User->id = $id_notifier;

			$visited = $this->User->read();

			$visiter = $this->User->find('first', array('fields' => array("DISTANCE(" . $visited['User']['pos_lat'] . ", " . $visited['User']['pos_long'] . ", pos_lat, pos_long) AS distance"),
				'conditions' => array('User.id' => $id_notified)));

			$images = $this->UserImages->find('all', array('fields' => array('position'),
				'conditions' => array('UserImages.user_id' => $visited['User']['id']),
				'limit' => 4));

			$photos = "";

			foreach($images as $keyF => $image)
			{
				$photos .= $image['UserImages']['position'];
			}

			$message = $visited['User']['name'] . ' ' . $notification['Notification']['body'];

			$passphrase = 'push40st';

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/Controller/ck.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			$body['aps'] = array(
				'alert' => array(
					'body' => $message,
					'action-loc-key' => 'Conferir'
					),
				'sound' => 'default',
				'badge' => '1',
				'matcher' => $id_notifier,
				'matched' => $id_notified,
				'idfacebook' => $visited['User']['facebook_id'],
				'photos' => $photos,
				'name' => $visited['User']['first_name'],
				'desc' => $visited['User']['description'],
				'age' => $visited['User']['birthday'],
				'dist' => round($visiter[0]['distance'])
				);

			$payload = json_encode($body);

			foreach($tokens as $keyT => $token)
			{
				$deviceToken = $token['DeviceInfos']['device_token'];

				$fp = stream_socket_client(
					'ssl://gateway.sandbox.push.apple.com:2195', $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

				if (!$fp)
					exit("Falha na conexao: $err $errstr" . PHP_EOL);

				echo 'Conectado ao APNS' . PHP_EOL;

				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

				$result = fwrite($fp, $msg, strlen($msg));

				if (!$result)
					$json = array('status' => 500);
				else
					$json = array('status' => 200);

				fclose($fp);                                                               

				echo json_encode($json);
			}   

			$this->Notification->id = $notification['Notification']['id'];
			$this->Notification->delete();
		}
	}

	public function visitperfil()
	{
		if($this->request->is('post'))
		{

			App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
			$facebook = new Facebook(array(
				'appId'  => Configure::read("FB_APP_ID"),
				'secret' => Configure::read("FB_APP_SECRET"),
				));

			$user = $this->request->data;
			$this->User->id = $user['User']['matched'];

			$matched = $this->User->read();

			$facebook->setAccessToken($matched['User']['facebook_token']);
			
			$commons = $facebook->api('/' . $user['User']['matcher'] . '?fields=context.fields(mutual_likes),context.fields(mutual_friends)');
			
			$result['User']['commons'] = $commons;

			$json = array('status' => 200, 'result' => $result);
		}
		else
		{
			$json = array('status' => 500);
		}
		
		echo json_encode($json);
	}

	public function returnmatches()
	{
		if($this->request->is('get'))
		{
			$user = $this->request->data;
			$user['User']['id'] = $_GET['user_id'];

			$this->loadModel('Match');

			$matches = array();
			$results = $this->Match->find('all', array(
				'joins' => array(
					array(
						'table' => 'talks',
						'alias' => 'talks',
						'type' => 'left',
						'conditions' => array(
							'talks.match_id = Match.id'
							)
						)
					),
				'conditions' => array(
					'OR' => array(
						'Match.matcher' => $user['User']['id'],
						'Match.matched' => $user['User']['id']
						)
					),
				'order' => array('talks.created asc'),
				'group' => array('Match.id')
				)
			);
			//echo('<PRE>');print_r($results);exit;

        	// corrige duplicatas
        	// var
			/*$resultsOK = array();
			$lista_rep = array();
			$myid = $user['User']['id'];
        	// guarda duplicatas e unicos na lista de key
			foreach ($results as $key => $value) {
				$matcher = $value['Match']['matcher'];
				$matched = $value['Match']['matched'];
				if($myid == $matcher){
					$lista_rep[$matched]['matcheR'] = $key;
				} elseif($myid == $matched){
					$lista_rep[$matcher]['matcheD'] = $key;	        			
				}
			}
			//echo('<PRE>');print_r($lista_rep);exit;
	        	// verifica a lista de keys
			foreach ($lista_rep as $key_id => $value) {
				if(!empty($value['matcheR']) AND !empty($value['matcheD'])){
	        			// verifica das duplicatas os itens que tem talks como prioridade
					$itemA = $results[$value['matcheR']];
					$itemB = $results[$value['matcheD']];
					if(count($itemA['Talk'])){
						$resultsOK[] = $itemA;
					} else {
						if(count($itemB['Talk'])){
							$resultsOK[] = $itemB;
						} else {
							$resultsOK[] = $itemA;
						}
					}
				} else {
	        			// guarda os itens unicos
					if(!empty($value['matcheR'])){
						$resultsOK[] = $results[$value['matcheR']];
					} elseif(!empty($value['matcheD'])){
						$resultsOK[] = $results[$value['matcheD']];
					}	        			
				}
			}*/
			echo('<PRE>');print_r($results);exit;

			foreach($resultsOK as $keyM => $result)
			{
				if($result['Match']['matcher'] == $user['User']['id'])
				{
					$match = $this->User->find('first', array('fields' => array(
						'id',
						'first_name',
						'last_name',
						'facebook_id'
						),
					'conditions' => array(
						'User.id' => $result['Match']['matched']
						)
					));

					$matches[] = array(
						'id' => $match['User']['id'],
						'match_id' => $result['Match']['id'],
						'name' => $match['User']['first_name'] . " " . $match['User']['last_name'],
						'facebook' => $match['User']['facebook_id']
						);
				}

				if($result['Match']['matched'] == $user['User']['id'])
				{
					$match = $this->User->find('first', array('fields' => array(
						'id',
						'first_name',
						'last_name',
						'facebook_id'
						),
					'conditions' => array(
						'User.id' => $result['Match']['matcher']
						)
					));

					$matches[] = array(
						'id' => $match['User']['id'],
						'match_id' => $result['Match']['id'],
						'name' => $match['User']['first_name'] . " " . $match['User']['last_name'],
						'facebook' => $match['User']['facebook_id']
						);
				}
			}

			$json = array('status' => 200, 'matches' => $matches);
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function dounmatch()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			$this->loadModel('Match');

			$match = $this->Match->find('first', array(
				'conditions' => array(
					'OR' => array(
						array(
							'AND' => array(
								'Match.matcher' => $user['Match']['unmatcher'],
								'Match.matched' => $user['Match']['unmatched']
								)
							),
						array(
							'AND' => array(
								'Match.matcher' => $user['Match']['unmatched'],
								'Match.matched' => $user['Match']['unmatcher']
								)
							)
						)
					)
				)
			);

			$this->Match->id = $match['Match']['id'];

			$this->Match->delete();
			
			$json = array('status' => 200);	
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function teste()
	{
			/*
			App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
		    $facebook = new Facebook(array(
		      'appId'  => Configure::read("FB_APP_ID"),
		      'secret' => Configure::read("FB_APP_SECRET"),
		    ));

		    $facebook->setAccessToken('CAAUu5an6RjUBAO3WSZB1cPB0vsAIAMSkoYOVCga5ZB6WyPkXTQtjRpKCl6aREhsEMuwbEfd9m6Swm8xUFN1KCavCpuz1b9pGoTlrYdsL9OHtK6M9nS6dnkidtCsdesJs3qvOop4ThfP64PAiG0K5PyZC21sOftmPivKXzr5sUZAqhvrmM1HZA1C9iZALQGBCCnP0eBdh3cSvvic7dL0yx5');
		    */
		    $this->loadModel('Match');

		    $match = $this->Match->find('first', array(
		    	'conditions' => array(
		    		'OR' => array(
		    			array(
		    				'AND' => array(
		    					'Match.matcher' => 590,
		    					'Match.matched' => 8
		    					)
		    				),
		    			array(
		    				'AND' => array(
		    					'Match.matcher' => 8,
		    					'Match.matched' => 590
		    					)
		    				)
		    			)
		    		)
		    	)
		    );

		    $this->Match->id = $match['Match']['id'];

		    if($this->Match->delete())
		    {
		    	$json = array('status' => 200);					
		    }
		    else
		    {
		    	$json = array('status' => 500);
		    }

		    echo json_encode($match);     	

	        //var_dump($searchs);

	    	//$json = array('status' => 200, 'result' => $result);	    	

			/*$file = 'app/webroot/image/searchuser.txt';
			$post = json_encode($inter_values);

			$file = fopen($file, 'a');
			fwrite($file, $post);
			fclose($file);*/

			$log = $this->User->getDataSource()->getLog(false, false);
			debug($log);
		}

		public function status($id = null)
		{
			if(isset($id))
			{
				$userInteration = $this->UserInteration->hasAny(array('UserInteration.user_id' => $id, '(TIME_TO_SEC(TIMEDIFF(now(),  UserInteration.created))/60) <' => 10));
				if($userInteration)
				{
					echo json_encode(array('status' => '200', 'message' => 'online'));
					
					return true;
				}

			}

			echo json_encode(array('status' => '200', 'message' => 'offline'));

			return false;		
		}	
	}
	?>
