<?php
App::uses('FrontendsController', 'Controller');
class UsersController extends FrontendsController
{
	public $layout = 'mobile';
	public $helpers = array('Html','Form','Js' => array('Jquery'));
	public $name = 'Users';
	public $uses = array('UserInteration', 'User'); 
	public $components = array('Session');

	public function beforeFilter() 
	{
		parent::beforeFilter();
	}

	public function me()
	{
		phpinfo();/*
		if($this->request->is('post'))
		{
			$user = $this->request->data;

		}*/
	}
	
	public function add()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;				

			//echo json_encode($user);
			//exit;
			
			$conditions = array('User.facebook_id' => $user["User"]["facebook_id"]);

			if ($this->User->hasAny($conditions))
			{
				$user = $this->User->find('first', array('conditions' => $conditions));
				$conditionI = array('UserImage.user_id' => $user["User"]["id"]);
				$this->loadModel('UserImage');
				$userImage = $this->UserImage->find('all', array('conditions' => $conditionI));
				$array_images = array();
				$cI = 0;

				foreach($userImage as $keyI => $image)
				{
					$array_images[] = array("id" => $image["UserImage"]["id"] , "src" => Configure::read("SITE_PATH") . $image["UserImage"]["dir"] . $image["UserImage"]["filename"] );									 	
					$cI ++;
				}

				$count = $cI;

				$this->loadModel('DeviceInfos');

				$conditions = array(
					'DeviceInfos.user_id' => $user["User"]["id"],
					'DeviceInfos.device_id' => $user["User"]["device_id"]
					);

				if($this->DeviceInfos->hasAny($conditions))
				{						
					$device = $this->DeviceInfos->find('first', array('conditions' => $conditions));

					$this->DeviceInfos->read(null, $device["DeviceInfos"]["id"]);

					$this->DeviceInfos->set(array(
						'device_token' => $user['User']['device_token']
						));

					$this->DeviceInfos->save();
				}
				else
				{
					$conditionDevice = array(
						'DeviceInfos.device_id' => $user["User"]["device_id"]
						);
					if($this->DeviceInfos->hasAny($conditionDevice)){

						$device = $this->DeviceInfos->find('first', array('conditions' => $conditionDevice));

						$this->DeviceInfos->read(null, $device["DeviceInfos"]["id"]);

						$this->DeviceInfos->set(array(
						'user_id' => $user['User']['id'],
						'device_token' => $user['User']['device_token'],
						'device_type' => $user['User']['device_type']
						));
					}else{
						$this->DeviceInfos->set(array(
						'user_id' => $user['User']['id'],
						'device_id' => $user['User']['device_id'],
						'device_token' => $user['User']['device_token'],
						'device_type' => $user['User']['device_type']
						));
					}

					$this->DeviceInfos->save();
				}
				

				$json = array('status' => 200, 'user' => $user["User"], "images" => $array_images ,"count" => $count );
			}
			else if($this->User->save($user))
			{
				App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
				$facebook = new Facebook(array(
					'appId'  => Configure::read("FB_APP_ID"),
					'secret' => Configure::read("FB_APP_SECRET"),
					));

				$facebook->setAccessToken($user["User"]['facebook_token']);
				$user_profile = $facebook->api('/me');
				$user_info = $facebook->api('/me?fields=gender');
				$user = $this->User->read(null, $this->User->id);

				if($user_info["gender"] == "female")
				{
					$gender = "2";
				}
				else if($user_info["gender"] == "male")
				{
					$gender = "1";
				}

				$user_albums = $facebook->api('/me/albums');
				$dir = 'app/webroot/image/';
				$array_images = array();
				$cI = 0;

				foreach($user_albums['data'] as $keyA => $album )
				{
					if($album['name'] == "Profile Pictures")
					{
						$api_album = "/" . $album['id'] . "/photos";
						$user_photos = $facebook->api($api_album);

						for($keyF = 0; $keyF < 4; $keyF++)
						{
							if(isset($user_photos['data'][$keyF]))
							{
								$photo = $user_photos['data'][$keyF];

								$filename = $album['from']['id'] . '_' . $keyF . '.jpg';

								$url = $user_photos['data'][$keyF]['source'];
								$img = $dir . $filename;
								file_put_contents($img, file_get_contents($url));

								$this->loadModel('UserImage');
								$this->UserImage->set(array('id' => null,
									'filename' => $filename,
									'dir' => $dir,
									'mimetype' => null,
									'filesize' => filesize($img),
									'position' => $keyF,
									'user_id' => $user["User"]['id']));
								
								$this->UserImage->save();
								$image = $this->UserImage->read();
								$array_images[] = array("id" => $image["UserImage"]["id"] , "src" => Configure::read("SITE_PATH") . $image["UserImage"]["dir"] . $image["UserImage"]["filename"] );									 	
								$cI ++;
							}		 				
						}
					}
				}
				
				$count = $cI;

				$this->loadModel('DeviceInfos');

				$this->DeviceInfos->set(array(
					'user_id' => $user['User']['id'],
					'device_id' => $user['User']['device_id'],
					'device_token' => $user['User']['device_token'],
					'device_type' => $user['User']['device_type']
					));

				$this->DeviceInfos->save();

				$this->User->set(array(
					'gender' => $gender,
					'quant_photo' => $count
					));

				$this->User->save();

				$json = array('status' => 200, 'user' => $user["User"], "images" => $array_images, "count" => $count);
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}
		
		echo json_encode($json);
	}

	public function edit()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			if($this->User->hasAny(array('User.id' => $user['User']['id'])))
			{
				$this->User->id = $user['User']['id'];

				$this->User->set(array(
					'description' => $user["User"]["description"]
					));

				if($this->User->save())
				{
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}			
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function edituser()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			if($this->User->hasAny(array('User.id' => $user['User']['id'])))
			{
				$this->User->id = $user['User']['id'];

				$this->User->set(array(
					'description' => $user["User"]["description"],
					'visible' => $user["User"]["visible"],
					'interest' => $user["User"]["interest"],
					'distance' => $user["User"]["distance"],
					'min_age' => $user["User"]["min_age"],
					'max_age' => $user["User"]["max_age"],
					'show_newcomb' => $user["User"]["show_newcomb"],
					'show_msgs' => $user["User"]["show_msgs"],
					'show_age' => $user["User"]["show_age"],
					'agree' => $user["User"]["agree"]
					));

				if($this->User->save())
				{
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500, 'error' => print_r($this->User->validationErrors, true));
				}
			}
			else
			{
				$json = array('status' => 501, 'error' => 'Nenhum usuario encontrado');
			}
		}
		else
		{
			$json = array('status' => 502, 'error' => 'Conexao sem post');
		}

		echo json_encode($json);
	}

	public function updatecountphoto()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			if($this->User->hasAny(array('User.id' => $user['User']['id'])))
			{
				$this->User->id = $user['User']['id'];

				$this->User->set(array(
					'quant_photo' => $user["User"]["count"]
					));

				if($this->User->save())
				{
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function albums()
	{
		App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
		$facebook = new Facebook(array(
			'appId'  => Configure::read("FB_APP_ID"),
			'secret' => Configure::read("FB_APP_SECRET"),
			));

		if($this->request->is('post'))
		{
			$user = $this->request->data;

			$conditions = array('User.facebook_id' => $user["User"]['id']);
			if ($this->User->hasAny($conditions))
			{
				$user = $this->User->find('first', array('conditions' => $conditions));
				$facebook->setAccessToken($user['User']["facebook_token"]);
				$user_albums = $facebook->api('/me/albums');

				$albums = array();

				foreach($user_albums['data'] as $keyA => $album)
				{
					$api_album = "/" . $album['id'] . "/photos";
					$photos_album = $facebook->api($api_album);
					$photos = array();

					foreach($photos_album['data'] as $keyP => $photo)
					{
						$photos[] = array('id' => $photo['id'], 'source' => $photo['source']);
					}

					$albums[] = array('id' => $album['id'], 'name' => $album['name'], 'picture' => $photos_album['data'][0]['picture'], 'photos' => $photos);
				}

				$json = array('status' => 200, 'albums' => $albums);
			}
			else
			{
				$json = array('status' => 5002);
			}
		}
		else
		{
			$json = array('status' => 5003);
		}

		echo json_encode($json);
	}

	public function uploadphoto()
	{
		if($this->request->is('post'))
		{
			$dir = 'app/webroot/image/';
			$photo = $this->request->data;

			//var_dump($photo);

			if(isset($photo['UserImage']['user'])){
				//Imagem vinda do facebook
				$photo_user = $photo['UserImage']['user'];
				$photo_position = $photo['UserImage']['position'];
			}
			else{
				//imagem vinda do device
				$photo_user = $photo['user'];
				$photo_position = $photo['position'];
			}

			$this->loadModel('User');
			$this->loadModel('UserImage');					

			$conditions_user = array('User.facebook_id' => $photo_user);
			$user = $this->User->find('first', array('conditions' => $conditions_user));

			$conditions_user_image = array('UserImage.user_id' => $user['User']['id'], 'UserImage.position' => $photo_position);
			$image = $this->UserImage->find('first', array('conditions' => $conditions_user_image));

			if(isset($_FILES["file"]["tmp_name"]))
			{
				//echo "isset file";
				$filename = $photo_user . '_' . $photo_position . '.jpg';

				//$pathimage = $dir . $filename;
				$img = $dir . $filename;

				move_uploaded_file($_FILES["file"]["tmp_name"], $img);
			}
			else
			{
				//echo "not isset file";
				$filename = $photo_user . '_' . $photo_position . '.jpg';
				$url = str_replace(",","&",$photo['UserImage']['url']);

				$img = $dir . $filename;
				//move_uploaded_file($_FILES["file"]["tmp_name"], $img);
				file_put_contents($img, file_get_contents($url));
			}

			//echo count($image);
			//exit;

			if($image == null)
			{
				//echo "salvou image";
				//exit;
				$this->UserImage->set(array(
					'filename' => $filename,
					'dir' => $dir,
					'mimetype' => null,
					'filesize' => filesize($img),
					'position' => $photo_position,
					'user_id' => $user["User"]['id']
					));
				
				$this->UserImage->save();
			}
			
			$json = array('status' => 200);
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function delphoto()
	{
		if($this->request->is('post'))
		{
			$photo = $this->request->data;

			$dir = 'app/webroot/image/';
			$id = $photo['UserImage']['user'];
			$pos = $photo['UserImage']['position'];

			$filename = $id . "_" . $pos . ".jpg";
			$file = $dir . $filename;

			if(file_exists($file))
			{
				$this->loadModel('UserImage');

				$conditions = array('UserImage.filename' => $filename);
				$getItem = $this->UserImage->find('first', array('conditions' => $conditions));
				$id = $getItem['UserImage']['id'];

				if($this->UserImage->delete($id))
				{
					unlink($file);
					
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 300);
			}
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function job_create()
	{
		// algum CTP deve ser definido para o cake
		$this->render('searchuser');

		// trata a criacao de jobs dos matchs e o proprio match
		$this->match_job_create();

		// trata a criacao de jobs dos matchs e o proprio match
		$this->talks_job_create();
	}

	private function match_job_create()
	{
		$this->loadModel('Relation');
		$this->loadModel('DeviceInfo');
		$this->loadModel('Match');
		$this->loadModel('Job');

		// 1p query para relacionar todos os ultimos likes mutuos encontrados
		$query = "SELECT old.id, new.id, old.relater_id AS id_to, old.related_id AS id_from
					FROM (SELECT id, relater_id AS id_to, related_id AS id_from
							FROM relations
						   WHERE job = 0 AND
						   		 opt = 1
						   		 ) AS new
					JOIN relations AS old ON new.id_to = old.related_id AND old.relater_id = new.id_from
				   WHERE opt = 1 AND
				   		 job = 0";
		$new_match = $this->Relation->query($query);

		// varre a lista de novos match tentando adiconar estes na tabela de forma a nao se repetir
		foreach($new_match as $Item){
			// busca este match na tabela anste de criar
			$id_from = $Item['old']['id_from'];
			$id_to = $Item['old']['id_to'];
			$old_id = $Item['old']['id'];
			$new_id = $Item['new']['id'];

			$query = "SELECT id
						FROM matches
					   WHERE (matcher = $id_to AND matched = $id_from) OR
					   		 (matcher = $id_from AND matched = $id_to)";

			// verifica se ja existe este match
			if(count($this->Match->query($query)) == 0){
				// prepara o novo match
				$this->Match->create();
				$this->Match->set(array(
					'matcher' => $id_from,
					'matched' => $id_to
				));
				// cria o novo match
				$this->Match->save();
				if($this->Match->save()){
					$MatchID = $this->Match->id;
					// para os dois participantes do match
					$query = "SELECT id
								FROM device_infos
							   WHERE user_id = $id_from";
					$DevicesFrom = $this->DeviceInfo->query($query);

					foreach($DevicesFrom as $device){
						// cria um novo job
						$this->Job->create();
						$this->Job->set(array(
							'type' => 'm',
							'fk_table_id' => $MatchID,
							'fk_from_id' => $id_to,
							'fk_to_id' => $id_from,
							'device_id' => $device['device_infos']['id']
						));
						$this->Job->save();
					}
					// para os dois participantes do match
					$query = "SELECT id
								FROM device_infos
							   WHERE user_id = $id_to";
					$DevicesTo = $this->DeviceInfo->query($query);

					foreach($DevicesTo as $device){
						// cria um novo job
						$this->Job->create();
						$this->Job->set(array(
							'type' => 'm',
							'fk_table_id' => $MatchID,
							'fk_from_id' => $id_from,
							'fk_to_id' => $id_to,
							'device_id' => $device['device_infos']['id']
						));
						$this->Job->save();
					}

					// atualiza a quantidade de jobs criados para o item no caso match
					$this->Relation->id = $old_id;
					$this->Relation->set(array( 'job' => '1' ));
					$this->Relation->save();
					$this->Relation->id = $new_id;
					$this->Relation->set(array( 'job' => '1' ));
					$this->Relation->save();
				} else {
					// em caso de erro nas tabelas para impedir um loop infinito de criacao de job invalido este é descartado
					$this->Relation->id = $old_id;
					$this->Relation->set(array( 'job' => '-1' ));
					$this->Relation->save();
					$this->Relation->id = $new_id;
					$this->Relation->set(array( 'job' => '-1' ));
					$this->Relation->save();
				}
			}
		}
	}

	private function talks_job_create()
	{
		$this->loadModel('Talk');
		$this->loadModel('DeviceInfo');
		$this->loadModel('Job');

		// 1p query para relacionar todos os ultimos likes mutuos encontrados
		$query = "SELECT id, user_id AS id_from, receipt_id AS id_to
					FROM talks
				   WHERE job = 0 AND
				   		 status = 'save_sever'";
		$new_Talks = $this->Talk->query($query);

		// varre a lista de novos Talk
		foreach($new_Talks as $Item){
			// busca os devices que devem receber os Talk
			$query = "SELECT id
						FROM device_infos
					   WHERE user_id = ".$Item['talks']['id_to'];
			$DevicesTo = $this->DeviceInfo->query($query);

			foreach($DevicesTo as $device){
				// cria um novo job
				$this->Job->create();
				$this->Job->set(array(
					'type' => 'c',
					'fk_table_id' => $Item['talks']['id'],
					'fk_from_id' => $Item['talks']['id_from'],
					'fk_to_id' => $Item['talks']['id_to'],
					'device_id' => $device['device_infos']['id']
				));
				$this->Job->save();
			}

			// atualiza a quantidade de jobs criados para o item no caso talks
			$this->Talk->id = $Item['talks']['id'];
			$this->Talk->set(array( 'job' => '1' ));
			$this->Talk->save();
		}
	}

	public function job_execute()
	{
		// algum CTP deve ser definido para o cake
		$this->render('searchuser');

		// busca os jobs validos ativos
		$this->loadModel('Job');
		$query = "SELECT id
					FROM jobs
				   WHERE status > 0";
		$JobsToDo = $this->Job->query($query);
		if(count($JobsToDo) == 0){
			return;
		}

		// varre lista de jobs para executar de forma assincrona utilizando o close do curl para iniciar o proximo item sem aguardar resposta
		$url = 'http://40st-backoffice.webbyapp.com/index.php/users/assync_job_execute';
		$c = curl_init();

		foreach($JobsToDo as $job){

			$url_id = $url.'?id='.$job['jobs']['id'];
			curl_setopt($c, CURLOPT_URL, $url_id);
			curl_setopt($c, CURLOPT_HEADER, false);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c, CURLOPT_FRESH_CONNECT, true);
			curl_setopt( $c, CURLOPT_TIMEOUT, 0 );	// era 1!
			curl_exec( $c );
		}
		sleep(30);
	}

	public function assync_job_execute()
	{
		// algum CTP deve ser definido para o cake
		$this->render('searchuser');

		// verifica se foi passado um id valido
		if(!isset($_GET['id']) OR !is_numeric($_GET['id'])){
			return;
		}
		$job_id = $_GET['id'];

		// busca o job pelo id
		$this->loadModel('Job');		
		$query = "SELECT *
					FROM jobs
				   WHERE id = $job_id AND
				   		 status > 0
				   LIMIT 1";
		$job = $this->Job->query($query);
		if(count($job) == 0)
			return;

		// retira a primeira row
		$job = $job['0'];
		unset($job['0']);

		// query para reduzir uma tentativa de notificar device. a quantidade de tentativas é o valor default para status maior que 0
		$query = "UPDATE jobs
					 SET status = status -1
				   WHERE id = $job_id AND
				   		 status > 0";
		$this->Job->query($query);

		// bsuca devices que vao receber as notificacoes
		$this->loadModel('DeviceInfo');
		$dev = $this->DeviceInfo->findById( $job['jobs']['device_id'] );
		if(count($dev) == 0)
			return;

		// define o tipo de notificacao que sera feita
		if($job['jobs']['type'] == 'c'){
			$this->loadModel('Talk');
			$tmp = $this->Talk->findById($job['jobs']['fk_table_id']);
			$this->sendNotification(
				'C',
				$job['jobs']['id'],
				$tmp['Talk']['message'],
				$tmp['Talk']['created'],
				$job['jobs']['fk_from_id'],
				$job['jobs']['fk_to_id'],
				$job['jobs']['fk_table_id'],
				$tmp['Talk']['match_id'],
				$dev['DeviceInfo']['device_token']);
		} elseif($job['jobs']['type'] == 'm'){
			/*$this->loadModel('Match');
			$tmp = $this->Match->findById($job['fk_table_id']);*/
			$this->sendNotification(
				'M',
				$job['jobs']['id'],
				'',
				'',
				$job['jobs']['fk_from_id'],
				$job['jobs']['fk_to_id'],
				'',
				$job['jobs']['fk_table_id'],
				$dev['DeviceInfo']['device_token']);
		}
	}

	private function sendNotification($Type, $Job_ID, $Mgs, $Create, $From_ID, $To_ID, $TableChatID, $TableMatchID, $Device)
	{
		$this->loadModel('Job');
		// recupera informacoes do usuario que envia a notificacao
		$this->loadModel('User');

		$tmp = $this->User->findById($From_ID);
		// é orbrigatorio conseguir os dados deste usuario
		if(count($tmp) == 0){
			// invalida o job por erro no banco
			$this->Jobs->id = $Job_ID;
			$this->Jobs->set(array( 'status' => '-1' ));
			return;
		}
		// guarda somente o necessario
		$facebook_id = $tmp['User']['facebook_id'];
		$first_name = $tmp['User']['first_name'];
		unset($tmp);

		if($Type == 'M'){
			$Mgs = $first_name.' curtiu voce!';
		}

		// ATENCAO ESTE CODIGO ABAIXO SO SERVE PARA IPHONE!!!
		//	https://blog.serverdensity.com/how-to-build-an-apple-push-notification-provider-server-tutorial/
		$passphrase = 'push40st';
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/Controller/ck.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		$body['aps'] = array(
			'alert' => array(
				'body' => $Mgs,
				'action-loc-key' => 'Conferir'
				),
			'sound' => 'default',
			'badge' => '1',
			'newnotification' => '1',
			'from' => $From_ID,
			'to' => $To_ID,
			'message' => $Mgs,
			'created' => $Create,
			'type' => $Type,
			'table_chat' => $TableChatID,
			'table_match' => $TableMatchID,
			'idfacebook' => $facebook_id,
			'name' => $first_name
			);
		$payload = json_encode($body);

		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 15, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);	// era 60!

		if (!$fp)
			exit("Falha na conexao: $err $errstr" . PHP_EOL);

		//echo 'Conectado ao APNS' . PHP_EOL;

		$msg = chr(0) . pack('n', 32) . pack('H*', $Device) . pack('n', strlen($payload)) . $payload;

		$result = fwrite($fp, $msg, strlen($msg));

		fclose($fp);

		if($result){
			// finaliza o job
			// atualiza a quantidade de jobs criados para o item no caso talks
			$this->Job->id = $Job_ID;
			$this->Job->set(array( 'status' => '0' ));
			$this->Job->save();
		}
	}

	public function checkmatch()
	{
		$this->render('searchuser');
		if($this->request->is('post'))
		{
			$user = $this->request->data;
			/*$user['User']['id'] = 1035;
			$user['User']['testid'] = 1141;*/

			// ******************************************* myID
			if( isset($user['User']['id']) AND is_numeric($user['User']['id']) ){
				$myid = $user['User']['id'];
			} else {
				$json = array('status' => 500, 'result' => "user id INVALID");
				echo json_encode($json);
				return;
			}
			// ******************************************* testID
			if( isset($user['User']['testid']) AND is_numeric($user['User']['testid']) ){
				$testid = $user['User']['testid'];
			} else {
				$json = array('status' => 500, 'result' => "user teste id INVALID");
				echo json_encode($json);
				return;
			}

			$query = "SELECT * FROM relations WHERE relater_id = ". $testid ." AND related_id = ". $myid ." AND opt = 1 LIMIT 1";
			$result = $this->User->query($query);

			if(count($result)){
				$json = array('status' => 200, 'match' => true );

			} else {
				$json = array('status' => 200, 'match' => false );

			}

		} else {
			$json = array('status' => 500);
		}
		echo json_encode($json);
	}

	public function searchuser()
	{
		if($this->request->is('post'))
		{
			// pega solicitacao
			$user = $this->request->data;
			// perfil teste
			/*$user['User']['perfil'] = 'load';
			$user['User']['id'] = 1222;
			$user['User']['perfilid'] = 1351;
			$user['User']['pos_lat'] = 'aaa';//-22.9964830587964;
			$user['User']['pos_long'] = null;//-43.3162;
			// search teste */
			/*$user['User']['limit'] = 5;
			$user['User']['exclude_list'] = '934,935';
			$user['User']['id'] = 1141;
			$user['User']['offset'] = 928;
			$user['User']['pos_lat'] = -23.123;
			$user['User']['pos_long'] = -43.123;
			$user['User']['interest'] = 3;
			$user['User']['distance'] = 5000;
			$user['User']['min_age'] = 20;
			$user['User']['max_age'] = 100;	//	*/

			// ******************************************* exclude_list
			//$json = array('status' => 200, 'user' => $user );
			//echo json_encode($json);
			//exit;

			$exclude_list = '';
			if(isset($user['User']['exclude_list'])){
				$exclude_list_OK = true;
				foreach (explode(',', $user['User']['exclude_list']) as $value)
				{
					if(!is_numeric($value)){
						$exclude_list_OK = false;
						break;
					}

				}
				if($exclude_list_OK)
				{
					$exclude_list = $user['User']['exclude_list'];
					$exclude_list = "`User`.`id` NOT IN ($exclude_list) AND";
				}
			}
			// ******************************************* get perfil
			if(isset($user['User']['perfil'])){
				$perfil = $user['User']['perfil'];
			}
			// ******************************************* offset
			if(!isset($user['User']['offset'])){
				$offset = 0;
			} else {
				$offset = $user['User']['offset'];
				if(!is_numeric($offset)){
					$offset = 0;
				}
			}
			// ******************************************* ID
			if( isset($user['User']['id']) AND is_numeric($user['User']['id']) ){
				$myid = $user['User']['id'];
			} else {
				$json = array('status' => 500, 'result' => "user id INVALID");
				echo json_encode($json);
				return;
			}
			if(!isset($perfil)){
				// ******************************************* verifica e corrige o limite caso passado errado
				if(!isset($user['User']['limit'])){
					$limit = 5;
				} else {
					$limit = $user['User']['limit'];
					if(!is_numeric($limit) AND $limit > 10){
						$limit = 5;
					}
				}
				// ******************************************* gender
				// default value. nao filtra
				/*	$gender = null;
				if(!isset($user['User']['gender'])){
					/*$json = array('status' => 500, 'result' => "gender NOT pass");
					echo json_encode($json);
					return;*/
				/*	} else {
					$gender_tmp = $user['User']['gender'];
					if(!is_numeric($gender)){
						/*$json = array('status' => 500, 'result' => "gender NOT numeric: $gender");
						echo json_encode($json);
						return;*/
				/*	} else {
						$gender_tmp = (int) $gender_tmp;
						if(!in_array($gender_tmp, array(1, 2))){
							/*$json = array('status' => 500, 'result' => "gender Invalid: $gender");
							echo json_encode($json);
							return;*/
				/*		} else {
							// monta sql com gender
							$gender = '('.$gender_tmp.',3)';
							$gender = "User.interest IN " . $gender;
						}
					}
				}*/
				// ******************************************* idade
				// default value. nao filtra
				if( !isset($user['User']['min_age']) OR !isset($user['User']['max_age']) ){
					$json = array('status' => 500, 'result' => "min_age OR max_age NOT pass");
					echo json_encode($json);
					return;
				} else {
					if( !is_numeric($user['User']['min_age']) OR !is_numeric($user['User']['max_age']) ){
						$json = array('status' => 500, 'result' => "min_age OR max_age NOT numeric: $gender");
						echo json_encode($json);
						return;
					} else {
						$min_age = $user["User"]["min_age"];
						$max_age = $user["User"]["max_age"];
					}
				}
				// ******************************************* interest
				// default value. nao filtra
				$interest = null;
				if(!isset($user['User']['interest'])){
					$json = array('status' => 500, 'result' => "interest NOT pass");
					echo json_encode($json);
					return;
				} else {
					$interest_tmp = $user['User']['interest'];
					if(!is_numeric($interest_tmp)){
						$json = array('status' => 500, 'result' => "interest NOT numeric: $interest");
						echo json_encode($json);
						return;
					} else {
						$interest_tmp = (int) $interest_tmp;
						if(!in_array($interest_tmp, array(1, 2, 3))){
							$json = array('status' => 500, 'result' => "interest Invalid: $gender");
							echo json_encode($json);
							return;
						} else {
							// monta sql com interest
							if($interest_tmp == 3)
							{
								$interest = "('1', '2')";
							} else {
								$interest = "('$interest_tmp')";
							}
							//	$interest = "User.gender IN " . $interest;
						}
					}
				}
				// ******************************************* distance
				if( isset($user["User"]["distance"]) AND is_numeric($user["User"]["distance"]) AND $user["User"]["distance"] > 0 ){
					$distance = $user["User"]["distance"];
				} else {
					$json = array('status' => 500, 'result' => "distance NOT pass");
					echo json_encode($json);
					exit;
				}
			} else {
				// ******************************************* perfilid
				if( isset($user['User']['perfilid']) AND is_numeric($user['User']['perfilid']) ){
					$perfil_id = $user['User']['perfilid'];
				} else {
					$json = array('status' => 500, 'result' => "perfil id INVALID");
					echo json_encode($json);
					return;
				}
			}
			// ******************************************* pos_lat + pos_long
			if(!isset($user["User"]["pos_lat"]) OR !isset($user["User"]["pos_long"])
				OR !is_numeric($user["User"]["pos_lat"]) OR !is_numeric($user["User"]["pos_long"])){
				/*$latuser = 0;
				$longuser = 0;*/
				// recupera a ultima pos lat guardada no banco CASO a info passada é invalida
				$user_tmp = $this->User->findById($myid);
				$latuser = $user_tmp['User']['pos_lat'];
				$longuser = $user_tmp['User']['pos_long'];
				unset($user_tmp);

			} else {
				$latuser = $user["User"]["pos_lat"];
				$longuser = $user["User"]["pos_long"];
			}
			// verifica se vazio
			$latuser = (empty($latuser) == true) ? 'null' : $latuser;
			$longuser = (empty($longuser) == true) ? 'null' : $longuser;
			// corrige erro de tipo no mysql
			/*$latuser = "CAST('".$latuser."' AS SIGNED)";
			$longuser = "CAST('".$longuser."' AS SIGNED)";*/
			// verifica se chamada de perfil
			if(isset($perfil)){
				$sear = 'perfil 0º';
				$result['0'] = $this->User->find('first',
					array( 'fields' => array("*", "DISTANCE(". $latuser .", ". $longuser .", pos_lat, pos_long) AS diffdistance"),
							'conditions' => array("User.id =" => $perfil_id),
							'limit' => 1)
					);

			} else {
				// recupera token do usuario que pesquisa para utilziar o app do facebook
				$this->loadModel('Match');
				$SQL = "SELECT IF(`Match`.`matcher`=$myid, `Match`.`matched`, `Match`.`matcher`) AS `match_id`
						FROM `clicksof_40st`.`matches` AS `Match`
						WHERE
							`Match`.`matcher` = $myid OR
							`Match`.`matched` = $myid";
				$result = $this->Match->query( $SQL );
				$TMP_match_exclude_list = array();
				foreach ($result as $value)
				{
					$TMP_match_exclude_list[] = $value['0']['match_id'];
				}
				$match_exclude_list = '';
				if(count($TMP_match_exclude_list)){
					$match_exclude_list = implode(',', $TMP_match_exclude_list);
					$match_exclude_list = "`User`.`id` NOT IN ($match_exclude_list) AND";
					unset($TMP_match_exclude_list);
				}
				// echo('<PRE>');print_r($match_exclude_list);exit;
				// faz a busca dos usuarios
				$SQL = "SELECT
							`User`.*,
							DISTANCE( $latuser , $longuser , pos_lat, pos_long) AS diffdistance,
							`User`.`id` > $offset as bit_offset,
							EXISTS (SELECT id FROM relations WHERE relater_id = $myid AND related_id = `User`.`id` AND opt = 2 LIMIT 1) as bit_related
						FROM
							`clicksof_40st`.`users` AS `User`
						WHERE
							`User`.`visible` = '1' AND
							`User`.`id` != $myid AND
							$exclude_list
							$match_exclude_list
							`User`.`facebook_id` IS NOT NULL AND
							`User`.`facebook_id` > '0' AND
							`User`.`facebook_id` != 'undefined' AND
							TIMESTAMPDIFF(YEAR,birthday,curdate()) BETWEEN $min_age AND $max_age AND
							DISTANCE( $latuser , $longuser , pos_lat, pos_long) <= $distance AND
							`User`.`gender` IN $interest AND
							NOT EXISTS (SELECT id FROM relations WHERE relater_id = $myid AND related_id = `User`.`id` AND opt <> 2 LIMIT 1)
						ORDER BY bit_related ASC, bit_offset DESC, id ASC
						LIMIT $limit";
				$sear = 'busca 0º';
				//echo $sql;
				//exit;
				$result = $this->User->query( $SQL );
			}
			// echo('<PRE>');print_r($result);exit;
			// informa que nao achou ninguem
			if(!count($result)){
				$json = array('status' => 200, 'result' => array(), 'sear' => $sear );
				echo json_encode($json);
				return;
			}
			// ???
			$this->loadModel('UserImages');
			$this->loadModel('Relation');
			// chama funcoes para utilizar app do facebook
			App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
			$facebook = new Facebook(array(
				'appId'  => Configure::read("FB_APP_ID"),
				'secret' => Configure::read("FB_APP_SECRET"),
				));
			// recupera token do usuario que pesquisa para utilziar o app do facebook
			$SQL = "SELECT `User`.`facebook_token`
					FROM `clicksof_40st`.`users` AS `User`
					WHERE `User`.`id` = $myid
					LIMIT 1";
			$facebook_token = $this->User->query( $SQL );
			$facebook_token = $facebook_token['0']['User']['facebook_token'];
			$facebook->setAccessToken($facebook_token);
			// ******************************************* tentativa de reducao no tempo de resposta do search E limitecao de informacoes privadas sendo enviadas indiscriminadamente
			$users_list = array();
			foreach($result as $keyI => $users)
			{
				$photos = $this->UserImages->find('all', array('fields' => array('position'),
					'conditions' => array('UserImages.user_id' => $users['User']['id']),
					'limit' => 4));

				$match = $this->Relation->hasAny(array('Relation.relater_id' => $users['User']['id'],
														'Relation.related_id' => $myid,
														'Relation.opt' => 1));
				try
				{
					$commons = $facebook->api('/' . $users['User']['facebook_id'] . '?fields=context.fields(mutual_likes),context.fields(mutual_friends)');
					$result[$keyI]['User']['commons'] = $commons;
				}	
				catch(Exception $e)
				{
					$result[$keyI]['User']['commons'] = array();		        		
				}

				$result[$keyI]['User']['photos'] = $photos;
				$result[$keyI]['User']['match'] = $match;

				// teste de reducao de na rsposta do search
				$users_list[$keyI]['User'] = $result[$keyI]['User'];
				$users_list[$keyI]['0']['diffdistance'] = ($result[$keyI]['0']['diffdistance'] > 1) ? $result[$keyI]['0']['diffdistance'] : '1';
				unset($result[$keyI]); 
				// teste de correcao para seguranca do usuario
				$users_list[$keyI]['User'] = $this->secureUser($users_list[$keyI]['User']);
			}
			// echo('<PRE>');print_r($users_list);exit;
			$json = array('status' => 200, 'result' => $users_list, 'sear' => $sear );
		} else {
			$json = array('status' => 500);
		}
		echo json_encode($json);
	}

	private function secureUser($user){
		unset($user['salt']);
		unset($user['password']);
		unset($user['email']);
		unset($user['pos_long']);
		unset($user['pos_lat']);
		unset($user['facebook_token']);
		unset($user['device_token']);
		unset($user['device_type']);
		unset($user['device_id']);
		return $user;
	}

	public function updategeoposition()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			$latitude = $user['User']['pos_lat'];
			$longitude = $user["User"]['pos_long'];

			if(empty($latitude) OR empty($longitude) OR $latitude == 'null' OR $longitude == 'null'){
				$json = array('status' => 500, 'msg' => 'null params');

			} else {
				if($this->User->hasAny(array('User.id' => $user['User']['id'])))
				{
					$this->User->id = $user['User']['id'];
					$this->User->set(array(	
						'pos_lat' => $latitude,
						'pos_long' => $longitude
						)
					);
					
					if($this->User->save())
					{
						$json = array('status' => 200);
					}
					else
					{
						$json = array('status' => 500);
					}
				}
				else
				{
					$json = array('status' => 500);
				}
			}
		}
		else
		{
			$json = array('status' => 500);
		}
		
		echo json_encode($json);
	}

	public function deleteuser()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			if($this->User->hasAny(array('User.id' => $user['User']['id'])))
			{
				$this->User->id = $user['User']['id'];

				$this->loadModel('UserImages');

				$images = $this->UserImages->find('all', array('conditions' => array('user_id' => $user['User']['id']))); 

				foreach($images as $keyI => $image)
				{
					$filename = $image['UserImages']['dir'] . $image['UserImages']['filename'];

					if(file_exists($filename))
					{
						unlink($filename);		
					}
				}

				if($this->User->delete())
				{
					$json = array('status' => 200);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}
		
		echo json_encode($json);			
	}

	

	public function returnoptreport()
	{
		$this->loadModel('Optreport');
		
		$json = array('status' => 200, 'result' => $this->Optreport->find('all'));

		echo json_encode($json);
	}

	public function toreport()
	{
		if($this->request->is('post'))
		{
			$report = $this->request->data;

			$this->loadModel('Report');

			$this->Report->set(array(
				'reported_id' => $report["Report"]["reported_id"],
				'reporter_id' => $report["Report"]["reporter_id"],
				'opt_report' => $report["Report"]["opt_report"],
				'description' => $report["Report"]["description"]
				));

			if($this->Report->save())
			{
				$json = array('status' => 200, 'id' => $report["Report"]["id"]);
			}
			else
			{
				$json = array('status' => 500);
			}
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function setrelation()
	{
		if($this->request->is('post'))
		{
			$this->loadModel('Relation');
			$relation = $this->request->data;
			/*$relation["Relation"]["searcher_id"] = 924;
			$relation["Relation"]["searched_id"] = 1000;
			$relation["Relation"]["opt_relation"] = 1;*/

			if(empty($relation["Relation"]["searcher_id"]) OR
				empty($relation["Relation"]["searcher_id"]) OR
				!in_array($relation["Relation"]["opt_relation"], array('1','2','3')) ){

				$json = array('status' => 500, 'msg' => 'min params not pass');

			} else {
				$condRelation = array(
					'Relation.relater_id' => $relation["Relation"]["searcher_id"], 
					'Relation.related_id' => $relation["Relation"]["searched_id"]);

				$rel = $this->Relation->find('first', array('conditions' => $condRelation));

				if(empty($rel)){
					try{
						$this->Relation->set(array(
							'relater_id' => $relation["Relation"]["searcher_id"],
							'related_id' => $relation["Relation"]["searched_id"],
							'opt' => $relation["Relation"]["opt_relation"]
							));

						if($this->Relation->save())
						{
							$json = array( 'status' => 200 );
						}
						else
						{
							$json = array( 'status' => 500, 'msg' => 'cant insert' );
						}
					} catch (Exception $e) {
						$json = array( 'status' => 501, 'msg' => 'cant insert, E!' );
					}
				} else {
					$this->Relation->id = $rel["Relation"]["id"];
					//$this->Relation->set('opt',$relation["Relation"]["opt_relation"]); 
					$this->Relation->set(array( 'opt' => $relation["Relation"]["opt_relation"] ));

					if($this->Relation->save())
					{
						$json = array( 'status' => 200 );
					}
					else
					{
						$json = array( 'status' => 500, 'msg' => 'cant update' );
					}
				}
			}
		} else {
			$json = array('status' => 500, 'msg' => 'not post' );
		}
		echo json_encode($json);
	}

	public function creatematchnotification()
	{
		if($this->request->is('post'))
		{
			$this->loadModel('Match');
			$this->loadModel('Notification');
			$notification = $this->request->data;
/*$notification["Notification"]["notifier"] = 1000;
$notification["Notification"]["notified"] = 880;*/

			if(empty($notification["Notification"]["notifier"]) OR empty($notification["Notification"]["notified"])){
				$json = array('status' => 500);

			} else {
				$condRelation = array('Match.matcher' => $notification["Notification"]["notifier"],
					'Match.matched' => $notification["Notification"]["notified"]);

				$rel = $this->Match->find('first', array('conditions' => $condRelation));

				if(!empty($rel)){
					$this->Match->id = $rel["Match"]["id"];
				}
				$this->Match->set(array(
					'matcher' => $notification['Notification']['notifier'],
					'matched' => $notification['Notification']['notified']
				));
				
				try {
					if($this->Match->save())
					{
						$this->Notification->set(array(
							'notifier' => $notification['Notification']['notifier'],
							'notified' => $notification['Notification']['notified'],
							'body' => 'também curtiu você.'
							));

						if($this->Notification->save())
						{
							$json = array('status' => 200);
						}
						else
						{
							$json = array('status' => 500);
						}
					}
					else
					{
						$json = array('status' => 500);
					}
				} catch (Exception $e) {
					$json = array('status' => 500);
				}
			}				
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function sendmatchnotification()
	{
		$this->loadModel('User');
		$this->loadModel('Notification');
		$this->loadModel('DeviceInfos');
		$this->loadModel('UserImages');

		$pendents = $this->Notification->find('all', array('limit' => 10, "conditions" => array("Notification.status" => null)));

		foreach ($pendents as $keyP => $notification)
		{
			$id_notified = $notification['Notification']['notified'];
			$id_notifier = $notification['Notification']['notifier'];

			//echo "id_notified = " . $id_notified . " id_notifier = " . $id_notifier ."<br>";
			
			$tokens = $this->DeviceInfos->find('all', array(
				'fields' => array('device_token'),
				'conditions' => array('DeviceInfos.user_id' => $id_notified)
				));

			$this->User->id = $id_notifier;

			$visited = $this->User->read();

			$visiter = $this->User->find('first', array('fields' => array("DISTANCE(" . $visited['User']['pos_lat'] . ", " . $visited['User']['pos_long'] . ", pos_lat, pos_long) AS distance"),
				'conditions' => array('User.id' => $id_notified)));

			$images = $this->UserImages->find('all', array('fields' => array('position'),
				'conditions' => array('UserImages.user_id' => $visited['User']['id']),
				'limit' => 4));

			$photos = "";

			foreach($images as $keyF => $image)
			{
				$photos .= $image['UserImages']['position'];
			}

			$message = $visited['User']['name'] . ' ' . $notification['Notification']['body'];

			$passphrase = 'push40st';

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/Controller/ck.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			$body['aps'] = array(
				'alert' => array(
					'body' => $message,
					'action-loc-key' => 'Conferir'
					),
				'sound' => 'default',
				'badge' => '1',
				'matcher' => $id_notifier,
				'matched' => $id_notified,
				'idfacebook' => $visited['User']['facebook_id'],
				'photos' => $photos,
				'name' => $visited['User']['first_name'],
				'desc' => $visited['User']['description'],
				'age' => $visited['User']['birthday'],
				'dist' => round($visiter[0]['distance'])
				);

			$payload = json_encode($body);

			foreach($tokens as $keyT => $token)
			{
				$deviceToken = $token['DeviceInfos']['device_token'];

				$fp = stream_socket_client(
					'ssl://gateway.sandbox.push.apple.com:2195', $err,
					$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

				if (!$fp)
					exit("Falha na conexao: $err $errstr" . PHP_EOL);

				echo 'Conectado ao APNS' . PHP_EOL;

				$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

				$result = fwrite($fp, $msg, strlen($msg));

				if (!$result)
					$json = array('status' => 500);
				else
					$json = array('status' => 200);

				fclose($fp);                                                               

				echo json_encode($json);
			}   

			$this->Notification->id = $notification['Notification']['id'];
			$this->Notification->delete();
		}
	}

	public function visitperfil()
	{
		if($this->request->is('post'))
		{

			App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
			$facebook = new Facebook(array(
				'appId'  => Configure::read("FB_APP_ID"),
				'secret' => Configure::read("FB_APP_SECRET"),
				));

			$user = $this->request->data;
			$this->User->id = $user['User']['matched'];

			$matched = $this->User->read();

			$facebook->setAccessToken($matched['User']['facebook_token']);
			
			$commons = $facebook->api('/' . $user['User']['matcher'] . '?fields=context.fields(mutual_likes),context.fields(mutual_friends)');
			
			$result['User']['commons'] = $commons;

			$json = array('status' => 200, 'result' => $result);
		}
		else
		{
			$json = array('status' => 500);
		}
		
		echo json_encode($json);
	}

	public function returnmatches()
	{
		$matches = array();
		if($this->request->is('post'))
		{
			$user = $this->request->data;
			//$user['User']['id'] = $_GET['user_id'];
			//echo json_encode(array('status' => 200, 'matches' => $results));exit;
			$this->loadModel('Match');

			$matches = array();
			$results = $this->Match->find('all', array(
				'joins' => array(
					array(
						'table' => 'talks',
						'alias' => 'talks',
						'type' => 'left',
						'conditions' => array(
							'talks.match_id = Match.id'
							)
						)
					),
				'conditions' => array(
					'OR' => array(
						'Match.matcher' => $user['User']['id'],
						'Match.matched' => $user['User']['id']
						)
					),
				'order' => array('talks.created asc'),
				'group' => array('Match.id')
				)
			);
			
			foreach($results as $keyM => $result)
			{
				if($result['Match']['matcher'] == $user['User']['id'])
				{
					$match = $this->User->find('first', array('fields' => array(
						'id',
						'first_name',
						'last_name',
						'facebook_id'
						),
					'conditions' => array(
						'User.id' => $result['Match']['matched']
						)
					));

					$matches[] = array(
						'id' => $match['User']['id'],
						'match_id' => $result['Match']['id'],
						'name' => $match['User']['first_name'] . " " . $match['User']['last_name'],
						'facebook' => $match['User']['facebook_id']
						);
				}

				if($result['Match']['matched'] == $user['User']['id'])
				{
					$match = $this->User->find('first', array('fields' => array(
						'id',
						'first_name',
						'last_name',
						'facebook_id'
						),
					'conditions' => array(
						'User.id' => $result['Match']['matcher']
						)
					));

					$matches[] = array(
						'id' => $match['User']['id'],
						'match_id' => $result['Match']['id'],
						'name' => $match['User']['first_name'] . " " . $match['User']['last_name'],
						'facebook' => $match['User']['facebook_id']
						);
				}				
			}
			//echo json_encode($matches);
			$json = array('status' => 200, 'matches' => $matches);
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
		//echo json_encode(array('status' => 200, 'matches' => $matches));
	}

	public function dounmatch()
	{
		if($this->request->is('post'))
		{
			$user = $this->request->data;

			$this->loadModel('Match');

			$match = $this->Match->find('first', array(
				'conditions' => array(
					'OR' => array(
						array(
							'AND' => array(
								'Match.matcher' => $user['Match']['unmatcher'],
								'Match.matched' => $user['Match']['unmatched']
								)
							),
						array(
							'AND' => array(
								'Match.matcher' => $user['Match']['unmatched'],
								'Match.matched' => $user['Match']['unmatcher']
								)
							)
						)
					)
				)
			);

			$this->Match->id = $match['Match']['id'];

			$this->Match->delete();
			
			$json = array('status' => 200);	
		}
		else
		{
			$json = array('status' => 500);
		}

		echo json_encode($json);
	}

	public function teste()
	{
			/*
			App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
		    $facebook = new Facebook(array(
		      'appId'  => Configure::read("FB_APP_ID"),
		      'secret' => Configure::read("FB_APP_SECRET"),
		    ));

		    $facebook->setAccessToken('CAAUu5an6RjUBAO3WSZB1cPB0vsAIAMSkoYOVCga5ZB6WyPkXTQtjRpKCl6aREhsEMuwbEfd9m6Swm8xUFN1KCavCpuz1b9pGoTlrYdsL9OHtK6M9nS6dnkidtCsdesJs3qvOop4ThfP64PAiG0K5PyZC21sOftmPivKXzr5sUZAqhvrmM1HZA1C9iZALQGBCCnP0eBdh3cSvvic7dL0yx5');
		    */
		    $this->loadModel('Match');

		    $match = $this->Match->find('first', array(
		    	'conditions' => array(
		    		'OR' => array(
		    			array(
		    				'AND' => array(
		    					'Match.matcher' => 590,
		    					'Match.matched' => 8
		    					)
		    				),
		    			array(
		    				'AND' => array(
		    					'Match.matcher' => 8,
		    					'Match.matched' => 590
		    					)
		    				)
		    			)
		    		)
		    	)
		    );

		    $this->Match->id = $match['Match']['id'];

		    if($this->Match->delete())
		    {
		    	$json = array('status' => 200);					
		    }
		    else
		    {
		    	$json = array('status' => 500);
		    }

		    echo json_encode($match);     	

	        //var_dump($searchs);

	    	//$json = array('status' => 200, 'result' => $result);	    	

			/*$file = 'app/webroot/image/searchuser.txt';
			$post = json_encode($inter_values);

			$file = fopen($file, 'a');
			fwrite($file, $post);
			fclose($file);*/

			$log = $this->User->getDataSource()->getLog(false, false);
			debug($log);
		}

		public function status($id = null)
		{
			if(isset($id))
			{
				$userInteration = $this->UserInteration->hasAny(array('UserInteration.user_id' => $id, '(TIME_TO_SEC(TIMEDIFF(now(),  UserInteration.created))/60) <' => 10));
				if($userInteration)
				{
					echo json_encode(array('status' => '200', 'message' => 'online'));
					
					return true;
				}

			}

			echo json_encode(array('status' => '200', 'message' => 'offline'));

			return false;		
		}	
	}
?>