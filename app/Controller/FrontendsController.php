<?php
App::uses('AppController', 'Controller');
class FrontendsController extends AppController
{          
	public $uses = array('UserInteration', 'User');                 
	public function beforeFilter() 
	{
    	parent::beforeFilter();
        header('Access-Control-Allow-Origin: *');
        $this->__saveInteration();
        //$this->Auth->allow();		
	}	

	private function __saveInteration()
	{		
		if(isset($this->params['url']['user_id']))
		{
			$condUserId = array('User.id' => $this->params['url']['user_id']);
			$thereIsUserId = $this->User->hasAny($condUserId);

			if($thereIsUserId)
			{
				$this->UserInteration->create();
				$this->UserInteration->set('user_id', $this->params['url']['user_id']);
				
				$interation = array();

				if(isset($this->request->params['controller']))
					$interation["controller"] = $this->request->params['controller'];

				if(isset($this->request->params['action']))
					$interation["action"] = $this->request->params['action'];

				if(isset($this->request->data))
					$interation["data"] = $this->request->data;

				if(isset($this->request->query))
					$interation["query"] = $this->request->query;

				if($this->request->is('post'))
					$interation["request_method"] = 'post';
				else if($this->request->is('get'))
					$interation["request_method"] = 'get';
				else if($this->request->is('put'))
					$interation["request_method"] = 'put';
				else if($this->request->is('delete'))
					$interation["request_method"] = 'delete';
			
				$this->UserInteration->set('interation', json_encode($interation));

				$this->UserInteration->save();
			}
		}
	}

}

?>