<?php
App::uses('AppController', 'Controller');
class BackendsController extends AppController
{
    public $components = array(
        'Session',
        'Auth' => array(
            'loginAction' => array('controller' => 'admins', 'action' => 'login'),
            'loginRedirect' => array('controller' => 'user_backs', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'admins', 'action' => 'login')
        )
    );
}

?>