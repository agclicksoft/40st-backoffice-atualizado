<?php
	App::uses('FrontendsController', 'Controller');
	class ChatsController extends FrontendsController
	{  
	  	/*
			TODO: envio da notificação push quando criar uma nova mensagem
	  	*/
	  	public $name = 'Chats';
	  	public $uses = array('Talk', 'User', 'Match','UserAction','UserInteration');  
	  	public $layout = null;

	  	public function beforeFilter() 
	  	{
	    	parent::beforeFilter();
		}
		/*
		* Index
		* exibe todas as mensagem enviadas no match
		* params id (codigo identificador do match)
		*/
	  	public function index($id = null) 
	  	{   
	    	if(!isset($id))
	  		{
	  			$this->set('data',  array('status' => '500', 'message' => 'Nenhuma mensagem encontrada.'));
				$this->render('/General/SerializeJson/');
				return false;
	  		}

	  		$conditions= array('Talk.match_id' => $id);
	  		$order = array('Talk.created asc');
	  		
	  		$chats = $this->Talk->find('all', array('conditions' => $conditions, 'order' => $order, 'limit' => '100'));
	  		
	  		$data = array();

	  		foreach($chats as $value)
	  		{
	  			
	  			$data[] = array('id' => $value['Talk']['id'], 'match_id' => $value['Talk']['match_id'], 'sender_id' => $value['SenderTalk']['id'], 'sender' => $value['SenderTalk']['name'], 'receipt_id' => $value['RecipientTalk']['id'], 'receipt' => $value['RecipientTalk']['name'], 'message' => $value['Talk']['message'], 'status' => $value['Talk']['status'], 'date' => $value['Talk']['created']); 
	  		}

	  		
	  		$this->set('data', array('status' => '200', 'talks' => $data));
	    	$this->render('/General/SerializeJson/');
	  	}
	  	/*
		* loadtalks
		* exibe todas as mensagem de um determinado usuario
		* params id (codigo identificador do usuario)
		*/
	  	public function loadtalks($id = null) 
	  	{   
	    	if(!isset($id))
	  		{
	  			$this->set('data',  array('status' => '500', 'message' => 'Nenhuma mensagem encontrada.'));
				$this->render('/General/SerializeJson/');
				return false;
	  		}

	  		//$conditions= array('Talk.user_id' => $id);
	  		$conditions = 'Talk.user_id ='. $id .' OR Talk.receipt_id ='. $id;
	  		$order = array('Talk.created asc');
	  		
	  		$chats = $this->Talk->find('all', array('conditions' => $conditions, 'order' => $order, 'limit' => '5000'));
	  		
	  		$data = array();

	  		foreach($chats as $value)
	  		{
	  			$data[] = array('id' => $value['Talk']['id'], 'match_id' => $value['Talk']['match_id'], 'sender_id' => $value['SenderTalk']['id'], 'sender' => $value['SenderTalk']['name'], 'receipt_id' => $value['RecipientTalk']['id'], 'receipt' => $value['RecipientTalk']['name'], 'message' => $value['Talk']['message'], 'status' => $value['Talk']['status'], 'date' => $value['Talk']['created']); 
	  		}

	  		
	  		$this->set('data', array('status' => '200', 'talks' => $data));
	    	$this->render('/General/SerializeJson/');
	  	}
	  	/*
		* view
		* exibe todas as mensagem enviadas para um usuario determinado durante um match
		* params id (codigo identificador do match), user_id (usuario que deseja visualizar a mensagem)
		*/
	  	public function view($user_id = null, $id = null, $status = null, $talk = null) 
	  	{   

	  		if(!isset($user_id))
	  		{
	  			$this->set('data',  array('status' => '500', 'message' => 'Nenhuma mensagem encontrada.'));
				$this->render('/General/SerializeJson/');
				return false;
	  		}
	  		
	  		$conditions = array();
	  		$conditions['Talk.receipt_id'] = $user_id;

	  		if(isset($id))
	  		{
	  			$conditions['Talk.match_id'] = $id;
	  		}

	  		if(isset($status))
	  		{
	  			$conditions['Talk.status'] = $status;
	  		}

	  		if(isset($talk))
	  		{
	  			$conditions['Talk.id >'] = $talk;
	  		}

	  		$order = array('Talk.created desc');
	  		
	  		$chats = $this->Talk->find('all', array('conditions' => $conditions, 'order' => $order, 'limit' => '100'));
	  		
	  		$data = array();

	  		foreach($chats as $value)
	  		{
	  			
	  			//$data[] = array('id' => $value['Talk']['id'], 'sender' => $value['SenderTalk']['name'],'receipt' => $value['RecipientTalk']['name'], 'message' => $value['Talk']['message'], 'date' => $value['Talk']['created']); 
	  			$data[] = array('id' => $value['Talk']['id'], 'match_id' => $value['Talk']['match_id'], 'sender_id' => $value['SenderTalk']['id'], 'sender' => $value['SenderTalk']['name'], 'receipt_id' => $value['RecipientTalk']['id'], 'receipt' => $value['RecipientTalk']['name'], 'message' => $value['Talk']['message'], 'status' => $value['Talk']['status'], 'date' => $value['Talk']['created']); 
	  		}

	  		
	  		$this->set('data', array('status' => '200', 'talks' => $data));
	    	$this->render('/General/SerializeJson/');
	  	}
	  	/*
		* add
		* adiciona uma mensagem no match
		* params Talk[message], Talk[user_id], Talk[receipt_id], Talk[match_id]
		*/
		public function add() 
		{
			if($this->request->is('post'))
			{
				$chat = $this->request->data;

				if(!isset($chat['Talk']['message']))
				{					
					$this->set('data',  array('status' => '500', 'message' => 'Nenhuma mensagem enviada.'));
					$this->render('/General/SerializeJson/');
					return false;
				}

				if(!isset($chat['Talk']['user_id']) || !isset($chat['Talk']['receipt_id']) || !isset($chat['Talk']['match_id']))
				{
					$this->set('data',  array('status' => '500', 'message' => 'Erro no chat.'));
					$this->render('/General/SerializeJson/');
					return false;
				}

				$condUserId = array('User.id' => $chat['Talk']["user_id"]);
				$condRecepeitId = array('User.id' => $chat['Talk']["receipt_id"]);
				$condMatchId = array(	
										'Match.id' => $chat['Talk']["match_id"],
										array('OR' => array('Match.matcher' => $chat['Talk']["user_id"], 'Match.matched' => $chat['Talk']["user_id"])),
										array('OR' => array('Match.matcher' => $chat['Talk']["receipt_id"], 'Match.matched' => $chat['Talk']["receipt_id"]))
									);
				//var_dump($condMatchId);
				//exit;

				$thereIsUserId = $this->User->hasAny($condUserId);
				$thereIsRecepeitId = $this->User->hasAny($condRecepeitId);
				$thereIsMatchId = $this->Match->hasAny($condMatchId);

				if(!$thereIsUserId || !$thereIsRecepeitId || !$thereIsMatchId)
				{
					$this->set('data',  array('status' => '500', 'message' => 'Erro no chat (usuario não encontrado)dd'. $thereIsMatchId.'.'));
					$this->render('/General/SerializeJson/');
					return false;
				}

				//$match = $this->Match->find('first', array('conditions' => array('Article.id' => 1)));

				$chat['Talk']['status'] = 'save_sever';

				if($this->Talk->save($chat))
				{
					//$chat = $this->Talk->read(null, $this->Talk->id);
					$this->set('data', array('status' => '200', 'match_id' => $chat['Talk']['match_id'], 'talk_local' => $chat['Talk']['local'], 'talk_id' => $this->Talk->id));
					$this->render('/General/SerializeJson/');
					return true;
				}
			}

			$this->set('data', array('status' => '500', 'message' => 'Erro no chat.'));
			$this->render('/General/SerializeJson/');
			return false;	    	
		}

		public function action()
		{
			if($this->request->is('post'))
			{
				$chat = $this->request->data;
				
				//var_dump($chat);
				//exit;

				if(!isset($chat['UserAction']['user_id']) || !isset($chat['UserAction']['match_id']) || !isset($chat['UserAction']['action']))
				{
					$this->set('data', array('status' => '500', 'message' => 'Informe todos os campos'));
					$this->render('/General/SerializeJson/');
					return false;
				}
				
				$condUserId = array('User.id' => $chat['UserAction']["user_id"]);
				$condMatchId = array('Match.id' => $chat['UserAction']["match_id"], 
					array('OR' => array('Match.matcher' => $chat['UserAction']["user_id"], 'Match.matched' => $chat['UserAction']["user_id"])));
				
				$thereIsUserId = $this->User->hasAny($condUserId);
				$thereIsMatchId = $this->Match->hasAny($condMatchId);
				
				if(!$thereIsMatchId || !$thereIsUserId)
				{
					$this->set('data', array('status' => '500', 'message' => 'Usuario nao encontrado ou Match nao encontrado ou usuario nao faz parte do match.'));
					$this->render('/General/SerializeJson/');
					return false;
				}	

				if(isset($chat['UserAction']['action_id']))
				{
					$condUserAction = array("UserAction.id" => $chat['UserAction']['action_id'],
											"UserAction.user_id" => $chat['UserAction']['user_id'],
											"UserAction.match_id" => $chat['UserAction']['match_id']);
					$thereIsUserAction = $this->UserAction->hasAny($condUserAction);

					if($thereIsUserAction)
					{
						$this->UserAction->id = $chat['UserAction']['action_id'];
						$this->UserAction->set('active', 'false');
						$this->UserAction->save();

						$this->set('data', array('status' => '200', 'action_id' => $this->UserAction->id ));
						$this->render('/General/SerializeJson/');
						return true;
					}
				}

				if($this->UserAction->save($chat))
				{
					$this->set('data', array('status' => '200', 'action_id' => $this->UserAction->id));
					$this->render('/General/SerializeJson/');
					return true;
				}							
			}
			
			$this->set('data', array('status' => '500', 'message' => 'metodo utilizado nao permitido.'));
			$this->render('/General/SerializeJson/');
			return false;	
		}
		
		public function edit($id = null) 
		{	

			if($this->request->is('post'))
			{
				$chat = $this->request->data;
				
				

				$this->set('data',$this->Talk->changeStatus($chat, $id));
				//exit;
				$this->render('/General/SerializeJson/');
				return true;
			}

			$this->set('data', array('status' => '500', 'message' => 'metodo utilizado nao permitido.'));
			$this->render('/General/SerializeJson/');
			return false;
		}
	                
	               
		public function delete($id = null) 
		{
	    	$this->render('/General/SerializeJson/');
		}                       
	}
?>