<?php
	App::uses('BackendsController', 'Controller');
	class UserBacksController extends BackendsController
	{
		public $layout = 'index';
		public $helpers = array('Html','Form','Js' => array('Jquery'));
		public $name = 'UserBacks';
		public $uses = array('User');  
		public $components = array('Session');

		public function beforeFilter() 
	    {
	        parent::beforeFilter();
	    }

		public function index()
		{
			$this->helpers[] = 'Age';

			$conditions = array();
			//$conditions['User.valid_login'] = true;

		    if ($this->request->is('post')) 
		    {
		      $search = $this->request->data;      
		      
		      if(isset($search['Search']['name']) && $search['Search']['name'] != "")
		      {
		        $conditions['User.name like'] = $search['Search']['name']."%";
		      }
		      
		      if(isset($search['Search']['email']) && $search['Search']['email'] != "")
		      {
		        $conditions['User.email like'] = $search['Search']['email']."%";
		      }
		      
		      if(isset($search['Search']['gender']) && $search['Search']['gender'] != "")
		      {
		        //$conditions['User.gender'] = $search['Search']['gender'];
                 
                 if ($search['Search']['gender'] == 1)
		        		$conditions['User.gender'] = 1;
		        	

                 if ($search['Search']['gender'] == 2)
		        		$conditions['User.gender'] = 2;
		        
		       
		         if ($search['Search']['gender'] == 3)
		        		$conditions['User.gender'] = array(1,2);

		      }
		       if(isset($search['Search']['birthday']) && $search['Search']['birthday'] != "")
		      {
		        $conditions['TIMESTAMPDIFF(Year,User.birthday,now())'] = $search['Search']['birthday'];
		      }

		       if(isset($search['Search']['country_id']) && $search['Search']['country_id']!=0)
		      {
		        $conditions['User.country_id'] = $search['Search']['country_id'];
		      }

		      if(isset($search['Search']['state_id'])  && $search['Search']['state_id']!=0)
		      {
		        $conditions['User.state_id'] = $search['Search']['state_id'];
		      }

		       if(isset($search['Search']['facebook_id']) && $search['Search']['facebook_id'] != "")
		      {
		      	    if ($search['Search']['facebook_id'] == 1)
		        	$conditions['User.facebook_id'] = null;
		    	
		    	 
                    if ($search['Search']['facebook_id'] == 2)
		    		$conditions['not'] =  array('User.facebook_id' => null);	    		    			
	    				    		
		      }

		    }		
            

            $options = array(
				'conditions' => $conditions, 
				'order' => array('User.created' => 'desc'),
				'limit' => 10
			);

			$this->paginate = $options;

			// Roda a consulta, já trazendo os resultados paginados
			$users = $this->paginate('User');
			               
			//$users = $this->User->find('all',array('conditions' => $conditions, 'order' => array('User.created' => 'desc')));
            $this->set('users', $users);
			
			$countries = $this->User->Country->find('list');
            $this->set(compact('countries'));

            $states = $this->User->State->find('list');
            $this->set(compact('states'));
                       
		}		
	}
?>
