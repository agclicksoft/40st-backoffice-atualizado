<?php
	App::uses('FrontendsController', 'Controller');
	class UsersController extends FrontendsController
	{
		public $layout = 'mobile';
		public $helpers = array('Html','Form','Js' => array('Jquery'));
		public $name = 'Users';
		public $uses = array('UserInteration', 'User'); 
		public $components = array('Session');

		public function beforeFilter() 
	    {
	        parent::beforeFilter();
	    }
		
		public function add()
		{
			if($this->request->is('post'))
			{
				$user = $this->request->data;				

				$conditions = array('User.facebook_id' => $user["User"]["facebook_id"]);

				if ($this->User->hasAny($conditions))
				{
					$user = $this->User->find('first', array('conditions' => $conditions));
					$conditionI = array('UserImage.user_id' => $user["User"]["id"]);
					$this->loadModel('UserImage');
					$userImage = $this->UserImage->find('all', array('conditions' => $conditionI));
					$array_images = array();
					$cI = 0;

					foreach($userImage as $keyI => $image)
					{
						$array_images[] = array("id" => $image["UserImage"]["id"] , "src" => Configure::read("SITE_PATH") . $image["UserImage"]["dir"] . $image["UserImage"]["filename"] );									 	
						$cI ++;
					}

					$count = $cI;

					$this->loadModel('DeviceInfos');

					$conditions = array(
											'DeviceInfos.user_id' => $user["User"]["id"],
											'DeviceInfos.device_id' => $user["User"]["device_id"]
										);

					if($this->DeviceInfos->hasAny($conditions))
					{						
						$device = $this->DeviceInfos->find('first', array('conditions' => $conditions));

						$this->DeviceInfos->read(null, $device["DeviceInfos"]["id"]);

						$this->DeviceInfos->set(array(
														'device_token' => $user['User']['device_token']
													));

						$this->DeviceInfos->save();
					}
					else
					{
						$this->DeviceInfos->set(array(
					 									'user_id' => $user['User']['id'],
					 								  	'device_id' => $user['User']['device_id'],
					 								 	'device_token' => $user['User']['device_token'],
					 								 	'device_type' => $user['User']['device_type']
					 								  ));

					 	$this->DeviceInfos->save();
					}
					 	

					$json = array('status' => 200, 'user' => $user["User"], "images" => $array_images ,"count" => $count );
				}
				else if($this->User->save($user))
				{
					App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
				    $facebook = new Facebook(array(
				      'appId'  => Configure::read("FB_APP_ID"),
				      'secret' => Configure::read("FB_APP_SECRET"),
				    ));

				    $facebook->setAccessToken($user["User"]['facebook_token']);
	 			    $user_profile = $facebook->api('/me');
					$user_info = $facebook->api('/me?fields=gender');
					$user = $this->User->read(null, $this->User->id);

					if($user_info["gender"] == "female")
					{
						$gender = "2";
					}
					else if($user_info["gender"] == "male")
					{
						$gender = "1";
					}

					$user_albums = $facebook->api('/me/albums');
				 	$dir = 'app/webroot/image/';
				 	$array_images = array();
				 	$cI = 0;

					foreach($user_albums['data'] as $keyA => $album )
					{
						if($album['name'] == "Profile Pictures")
						{
							$api_album = "/" . $album['id'] . "/photos";
				 			$user_photos = $facebook->api($api_album);

				 			for($keyF = 0; $keyF < 4; $keyF++)
				 			{
				 				if(isset($user_photos['data'][$keyF]))
				 				{
				 					$photo = $user_photos['data'][$keyF];

					 				$filename = $album['from']['id'] . '_' . $keyF . '.jpg';

				 					$url = $user_photos['data'][$keyF]['source'];
									$img = $dir . $filename;
									file_put_contents($img, file_get_contents($url));

									$this->loadModel('UserImage');
									$this->UserImage->set(array('id' => null,
																'filename' => $filename,
														   		'dir' => $dir,
														   		'mimetype' => null,
														   		'filesize' => filesize($img),
														   		'position' => $keyF,
														   		'user_id' => $user["User"]['id']));
									
									$this->UserImage->save();
									$image = $this->UserImage->read();
									$array_images[] = array("id" => $image["UserImage"]["id"] , "src" => Configure::read("SITE_PATH") . $image["UserImage"]["dir"] . $image["UserImage"]["filename"] );									 	
				 					$cI ++;
				 				}		 				
				 			}
				 		}
				 	}
				 	
				 	$count = $cI;

				 	$this->loadModel('DeviceInfos');

				 	$this->DeviceInfos->set(array(
				 									'user_id' => $user['User']['id'],
				 								  	'device_id' => $user['User']['device_id'],
				 								 	'device_token' => $user['User']['device_token'],
				 								 	'device_type' => $user['User']['device_type']
				 								 ));

				 	$this->DeviceInfos->save();

				 	$this->User->set(array(
		 									'gender' => $gender,
		 								  	'quant_photo' => $count
		 								 ));

				 	$this->User->save();

					$json = array('status' => 200, 'user' => $user["User"], "images" => $array_images, "count" => $count);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}
			
			echo json_encode($json);
		}

		public function edit()
		{
			if($this->request->is('post'))
			{
				$user = $this->request->data;

				if($this->User->hasAny(array('User.id' => $user['User']['id'])))
				{
					$this->User->id = $user['User']['id'];

					$this->User->set(array(
												'description' => $user["User"]["description"]
											));

					if($this->User->save())
					{
						$json = array('status' => 200);
					}
					else
					{
						$json = array('status' => 500);
					}
				}
				else
				{
					$json = array('status' => 500);
				}			
			}
			else
			{
				$json = array('status' => 500);
			}

			echo json_encode($json);
		}

		public function edituser()
		{
			if($this->request->is('post'))
			{
				$user = $this->request->data;

				if($this->User->hasAny(array('User.id' => $user['User']['id'])))
				{
					$this->User->id = $user['User']['id'];

					$this->User->set(array(
											'description' => $user["User"]["description"],
											'visible' => $user["User"]["visible"],
											'interest' => $user["User"]["interest"],
											'distance' => $user["User"]["distance"],
											'min_age' => $user["User"]["min_age"],
											'max_age' => $user["User"]["max_age"],
											'show_newcomb' => $user["User"]["show_newcomb"],
											'show_msgs' => $user["User"]["show_msgs"],
											'show_age' => $user["User"]["show_age"],
											'agree' => $user["User"]["agree"]
										));

					if($this->User->save())
					{
						$json = array('status' => 200);
					}
					else
					{
						$json = array('status' => 500, 'error' => print_r($this->User->validationErrors, true));
					}
				}
				else
				{
					$json = array('status' => 501, 'error' => 'Nenhum usuario encontrado');
				}
			}
			else
			{
				$json = array('status' => 502, 'error' => 'Conexao sem post');
			}

			echo json_encode($json);
		}

		public function updatecountphoto()
		{
			if($this->request->is('post'))
			{
				$user = $this->request->data;

				if($this->User->hasAny(array('User.id' => $user['User']['id'])))
				{
					$this->User->id = $user['User']['id'];

					$this->User->set(array(
											'quant_photo' => $user["User"]["count"]
										   ));

					if($this->User->save())
					{
						$json = array('status' => 200);
					}
					else
					{
						$json = array('status' => 500);
					}
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}

			echo json_encode($json);
		}

		public function albums()
		{
			App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
		    $facebook = new Facebook(array(
		      'appId'  => Configure::read("FB_APP_ID"),
		      'secret' => Configure::read("FB_APP_SECRET"),
		    ));

		    if($this->request->is('post'))
		    {
		    	$user = $this->request->data;

		    	$conditions = array('User.facebook_id' => $user["User"]['id']);

				if ($this->User->hasAny($conditions))
				{
					$user = $this->User->find('first', array('conditions' => $conditions));
					$facebook->setAccessToken($user['User']["facebook_token"]);
		 			$user_albums = $facebook->api('/me/albums');

		 			$albums = array();

		 			foreach($user_albums['data'] as $keyA => $album)
		 			{
		 				$api_album = "/" . $album['id'] . "/photos";
		 				$photos_album = $facebook->api($api_album);
		 				$photos = array();

		 				foreach($photos_album['data'] as $keyP => $photo)
		 				{
		 					$photos[] = array('id' => $photo['id'], 'source' => $photo['source']);
		 				}

		 				$albums[] = array('id' => $album['id'], 'name' => $album['name'], 'picture' => $photos_album['data'][0]['picture'], 'photos' => $photos);
		 			}

		 			$json = array('status' => 200, 'albums' => $albums);
				}
				else
				{
					$json = array('status' => 500);
				}
		    }
		    else
		    {
		    	$json = array('status' => 500);
		    }

		    echo json_encode($json);
		}

		public function uploadphoto()
		{
			if($this->request->is('post'))
			{
				$dir = 'app/webroot/image/';
				$photo = $this->request->data;

				$this->loadModel('User');
				$this->loadModel('UserImage');					

				$conditions = array('User.facebook_id' => $photo['UserImage']['user']);
				$user = $this->User->find('first', array('conditions' => $conditions));

				$conditions = array('UserImage.user_id' => $user['User']['id'], 'UserImage.position' => $photo['UserImage']['position']);
				$image = $this->UserImage->find('first', array('conditions' => $conditions));

				if(isset($_FILES["file"]["tmp_name"]))
				{
					$filename = $_POST["user"] . '_' . $_POST['position'] . '.jpg';

					$pathimage = $dir . $filename;

					move_uploaded_file($_FILES["file"]["tmp_name"], $pathimage);
				}
				else
				{
					$filename = $photo['UserImage']['user'] . '_' . $photo['UserImage']['position'] . '.jpg';
					$url = str_replace(",","&",$photo['UserImage']['url']);

					$img = $dir . $filename;
					file_put_contents($img, file_get_contents($url));
				}

				if($image == null)
				{
					$this->UserImage->set(array(
													'filename' => $filename,
											   		'dir' => $dir,
											   		'mimetype' => null,
											   		'filesize' => filesize($img),
											   		'position' => $photo['UserImage']['position'],
											   		'user_id' => $user["User"]['id']
										   		));
					
					$this->UserImage->save();
				}
								
				$json = array('status' => 200);
			}
		    else
		    {
		    	$json = array('status' => 500);
		    }

		    echo json_encode($json);
		}

		public function delphoto()
		{
			if($this->request->is('post'))
			{
				$photo = $this->request->data;

				$dir = 'app/webroot/image/';
				$id = $photo['UserImage']['user'];
				$pos = $photo['UserImage']['position'];

				$filename = $id . "_" . $pos . ".jpg";
				$file = $dir . $filename;

				if(file_exists($file))
				{
					$this->loadModel('UserImage');

					$conditions = array('UserImage.filename' => $filename);
					$getItem = $this->UserImage->find('first', array('conditions' => $conditions));
					$id = $getItem['UserImage']['id'];

					if($this->UserImage->delete($id))
					{
						unlink($file);
					
						$json = array('status' => 200);
					}
					else
					{
						$json = array('status' => 500);
					}
				}
				else
				{
					$json = array('status' => 300);
				}
			}
			else
			{
				$json = array('status' => 500);
			}

			echo json_encode($json);
		}

		public function searchuser()
		{
			if($this->request->is('post'))
			{
				$user = $this->request->data;

				/*$user["User"]['id'] = "880";
				$user["User"]['limit'] = 0;
				$user["User"]['offset'] = 5;*/

				if(!isset($user["User"]['id'])){
					$json = array('status' => 500, 'result' => "user id NOT pass");
					echo json_encode($json);
					return;
				}

				App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
				$facebook = new Facebook(array(
					'appId'  => Configure::read("FB_APP_ID"),
					'secret' => Configure::read("FB_APP_SECRET"),
					));

				$this->User->id = $user['User']['id'];
				$limit = $user['User']['limit'];
				$offset = $user['User']['offset'];
				if(isset($user['User']['perfil'])){
					$perfil = $user['User']['perfil'];
				}

				/*$where_perfil = "NOT EXISTS (SELECT id FROM relations WHERE relater_id = " . $user['User']['id'] . " AND related_id = User.id and opt <> 2)";
				$where_offset = "User.id > " . $offset;
				if(isset($user['User']['perfil'])){
					$where_perfil = '';
					$where_offset = "User.id = " . $offset;
				}*/

				$user = $this->User->read();

				$facebook->setAccessToken($user["User"]['facebook_token']);

				$gender = '('.$user["User"]["gender"].',3)';
				$gender = "User.interest IN " . $gender;

				$interest = $user["User"]["interest"];
				if($interest == '3')
				{
					$interest = '(1, 2)';
				} else {
					$interest = '('.$interest.')';
				}
				$interest = "User.gender IN " . $interest;

				$latuser = $user["User"]["pos_lat"];
				$longuser = $user["User"]["pos_long"];

				// define valores default
				$latuser = (empty($latuser) == true) ? 'null' : $latuser;
				$longuser = (empty($longuser) == true) ? 'null' : $longuser;
				// corrige problema de conversao de valores que quebra a
				// query no banco impedindo a funcao DISTANCE de funcionar
				//if($latuser != 'null')
					$latuser = "CAST('".$latuser."' AS SIGNED)";
				//if($longuser != 'null')
					$longuser = "CAST('".$longuser."' AS SIGNED)";
				$limit = 3;//(empty($limit) == true) ? '10' : $limit;

				if(isset($perfil)){
					$result = $this->User->find('all', array(
					'fields' => array("*", "DISTANCE(" . $latuser . ", " . $longuser . ", pos_lat, pos_long) AS diffdistance"), 
					'conditions' => array(
						"User.id =" => $offset
					), 'limit' => $limit));

				} else {
					// verifica se o offset esta acima do limite da tabela
					$offset = (int)$offset;
					$offset_where = '';
					$result = $this->User->find('all', array(
					'fields' => array("MAX(User.id) AS IDmax"), 
					'conditions' => array(), ));
					if(isset($result[0][0]['IDmax'])){
						$IDmax = (int)$result[0][0]['IDmax'];
						if(is_integer($IDmax)){
							if($IDmax > $offset){
								$offset_where = "User.id > ".$offset;
							}
						}
					}

					$result = $this->User->find('all', array(
					'fields' => array("*", "DISTANCE(" . $latuser . ", " . $longuser . ", pos_lat, pos_long) AS diffdistance"), 
					'conditions' => array(
						'User.visible' => '1',
						'User.id !=' => $user["User"]['id'],
						"`User`.`facebook_id` IS NOT NULL AND `User`.`facebook_id` > '0' AND `User`.`facebook_id` != 'undefined'",
						//"`User`.`facebook_id` IS NOT NULL AND `User`.`facebook_id` > '0'",
						$offset_where, //'User.id >' => $offset,
						"TIMESTAMPDIFF(YEAR,birthday,curdate()) BETWEEN ? AND ?" => array($user["User"]["min_age"]-1, $user["User"]["max_age"]+1),
						//"DISTANCE(" . $latuser . ", " . $longuser . ", pos_lat, pos_long) <= " => $user["User"]["distance"],
						$interest,
						$gender,
						//"EXISTS (SELECT id FROM relations WHERE relater_id = " . $user['User']['id'] . " AND related_id = User.id and opt = 7)"
						"NOT EXISTS (SELECT id FROM relations WHERE relater_id = " . $user['User']['id'] . " AND related_id = User.id and opt <> 2)"
					), 'limit' => $limit));
					
					if(!count($result)){
						$result = $this->User->find('all', array(
						'fields' => array("*", "DISTANCE(" . $latuser . ", " . $longuser . ", pos_lat, pos_long) AS diffdistance"), 
						'conditions' => array(
						'User.visible' => '1',
						'User.id !=' => $user["User"]['id'],
						"`User`.`facebook_id` IS NOT NULL AND `User`.`facebook_id` > '0' AND `User`.`facebook_id` != 'undefined'",
						//"`User`.`facebook_id` IS NOT NULL AND `User`.`facebook_id` > '0'",
						//'User.id >' => $offset,
						"TIMESTAMPDIFF(YEAR,birthday,curdate()) BETWEEN ? AND ?" => array($user["User"]["min_age"]-1, $user["User"]["max_age"]+1),
						//"DISTANCE(" . $latuser . ", " . $longuser . ", pos_lat, pos_long) <= " => $user["User"]["distance"],
						$interest,
						$gender,
						//"EXISTS (SELECT id FROM relations WHERE relater_id = " . $user['User']['id'] . " AND related_id = User.id and opt = 7)"
						"NOT EXISTS (SELECT id FROM relations WHERE relater_id = " . $user['User']['id'] . " AND related_id = User.id and opt <> 2)"
					), 'limit' => $limit));
					}
				}
				
				$this->loadModel('UserImages');
				$this->loadModel('Relation');

				// teste de reducao de na rsposta do serach
				$users_list = array();

				foreach($result as $keyI => $users)
				{
					$photos = $this->UserImages->find('all', array('fields' => array('position'),
						'conditions' => array('UserImages.user_id' => $users['User']['id']),
						'limit' => 4));

					$match = $this->Relation->hasAny(array('Relation.relater_id' => $users['User']['id'],
						'Relation.related_id' => $user["User"]['id'],
						'Relation.opt' => 1));
					try
					{
						$commons = $facebook->api('/' . $users['User']['facebook_id'] . '?fields=context.fields(mutual_likes),context.fields(mutual_friends)');		
						$result[$keyI]['User']['commons'] = $commons;
					}	
					catch(Exception $e)
					{
						$result[$keyI]['User']['commons'] = array();		        		
					}
					
					$result[$keyI]['User']['photos'] = $photos;
					$result[$keyI]['User']['match'] = $match;

					// teste de correcao para seguranca do usuario
					unset($result[$keyI]['User']['salt']);
					unset($result[$keyI]['User']['password']);
					unset($result[$keyI]['User']['email']);
					unset($result[$keyI]['User']['pos_long']);
					unset($result[$keyI]['User']['pos_lat']);
					// teste de reducao de na rsposta do serach
					$users_list[$keyI]['User'] = $result[$keyI]['User'];
					$users_list[$keyI]['0'] = $result[$keyI]['0'];
				}

				/*echo '<pre>';
				print_r($users_list);
				exit;*/

				$json = array('status' => 200, 'result' => $users_list);
				//$json = array('status' => 200, 'result' => $result);	        	
			}
			else
			{
				$json = array('status' => 500);
			}

			echo json_encode($json);
		}

		public function updategeoposition()
		{
			if($this->request->is('post'))
			{
				$user = $this->request->data;

				$latitude = $user['User']['pos_lat'];
				$longitude = $user["User"]['pos_long'];

				if($this->User->hasAny(array('User.id' => $user['User']['id'])))
				{
					$this->User->id = $user['User']['id'];

					$this->User->set(array(	
										'pos_lat' => $latitude,
								   		'pos_long' => $longitude
								   	   )
								);
					
					if($this->User->save())
					{
						$json = array('status' => 200);
					}
					else
					{
						$json = array('status' => 500);
					}
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}
        	
        	echo json_encode($json);
		}

		public function deleteuser()
		{
			if($this->request->is('post'))
			{
				$user = $this->request->data;

				if($this->User->hasAny(array('User.id' => $user['User']['id'])))
				{
					$this->User->id = $user['User']['id'];

					$this->loadModel('UserImages');

					$images = $this->UserImages->find('all', array('conditions' => array('user_id' => $user['User']['id']))); 

					foreach($images as $keyI => $image)
					{
						$filename = $image['UserImages']['dir'] . $image['UserImages']['filename'];

						if(file_exists($filename))
						{
							unlink($filename);		
						}
					}

					if($this->User->delete())
					{
						$json = array('status' => 200);
					}
					else
					{
						$json = array('status' => 500);
					}
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}
        	
        	echo json_encode($json);			
		}

		

		public function returnoptreport()
		{
			$this->loadModel('Optreport');
	        
			$json = array('status' => 200, 'result' => $this->Optreport->find('all'));

	    	echo json_encode($json);
		}

		public function toreport()
		{
			if($this->request->is('post'))
			{
				$report = $this->request->data;

				$this->loadModel('Report');

				$this->Report->set(array(
						'reported_id' => $report["Report"]["reported_id"],
						'reporter_id' => $report["Report"]["reporter_id"],
						'opt_report' => $report["Report"]["opt_report"],
						'description' => $report["Report"]["description"]
					));

				if($this->Report->save())
				{
					$json = array('status' => 200, 'id' => $report["Report"]["id"]);
				}
				else
				{
					$json = array('status' => 500);
				}
			}
			else
			{
				$json = array('status' => 500);
			}

			echo json_encode($json);
		}

		public function setrelation()
		{
			if($this->request->is('post'))
			{
				$relation = $this->request->data;

				$this->loadModel('Relation');


				$condRelation = array('Relation.relater_id' => $relation["Relation"]["searcher_id"], 
									'Relation.related_id' => $relation["Relation"]["searched_id"]);

				$thereIsRelation = $this->Relation->hasAny($condRelation);
				if(!$thereIsRelation)
				{
					$this->Relation->set(array(
							'relater_id' => $relation["Relation"]["searcher_id"],
							'related_id' => $relation["Relation"]["searched_id"],
							'opt' => $relation["Relation"]["opt_relation"]
						));

					if($this->Relation->save())
					{
						$json = array('status' => 200, 
							'id' => $relation["Relation"]["id"], 
							'matched' => $relation["Relation"]["searched_id"], 
							'opt' => $relation["Relation"]["opt_relation"]
						);
					}
					else
					{
						$json = array('status' => 500);
					}
				}
				else
				{
					$rel = $this->Relation->find('first', array('conditions' => $condRelation));

					$this->Relation->id = $rel["Relation"]["id"];
					$this->Relation->set('opt',$relation["Relation"]["opt_relation"]);

					if($this->Relation->save())
					{
						$json = array('status' => 200, 
							'id' => $relation["Relation"]["id"], 
							'matched' => $relation["Relation"]["searched_id"], 
							'opt' => $relation["Relation"]["opt_relation"]
						);
					}
					else
					{
						$json = array('status' => 500);
					}

				}
			}
			else
			{
				$json = array('status' => 500);
			}

			echo json_encode($json);
		}

		public function creatematchnotification()
		{
			if($this->request->is('post'))
			{
				$notification = $this->request->data;

				$this->loadModel('Match');
				$this->loadModel('Notification');

				$this->Match->set(array(
										'matcher' => $notification['Notification']['notifier'],
										'matched' => $notification['Notification']['notified']
									   ));

				if($this->Match->save())
				{
					$this->Notification->set(array(
													'notifier' => $notification['Notification']['notifier'],
													'notified' => $notification['Notification']['notified'],
													'body' => 'também curtiu você.'
												  ));

					if($this->Notification->save())
					{
						$json = array('status' => 200);
					}
					else
					{
						$json = array('status' => 500);
					}
				}
				else
				{
					$json = array('status' => 500);
				}				
			}
			else
			{
				$json = array('status' => 500);
			}

			echo json_encode($json);
		}

		public function sendmatchnotification()
		{
			$this->loadModel('User');
			$this->loadModel('Notification');
			$this->loadModel('DeviceInfos');
			$this->loadModel('UserImages');

			$pendents = $this->Notification->find('all', array('limit' => 10, "conditions" => array("Notification.status" => null)));

            foreach ($pendents as $keyP => $notification)
            {
                $id_notified = $notification['Notification']['notified'];
                $id_notifier = $notification['Notification']['notifier'];

                
                $tokens = $this->DeviceInfos->find('all', array(
                        											'fields' => array('device_token'),
                        											'conditions' => array('DeviceInfos.user_id' => $id_notified)
                        										));

                $this->User->id = $id_notifier;

				$visited = $this->User->read();

                $visiter = $this->User->find('first', array('fields' => array("DISTANCE(" . $visited['User']['pos_lat'] . ", " . $visited['User']['pos_long'] . ", pos_lat, pos_long) AS distance"),
                											'conditions' => array('User.id' => $id_notified)));

                $images = $this->UserImages->find('all', array('fields' => array('position'),
	        												   'conditions' => array('UserImages.user_id' => $visited['User']['id']),
	        												   'limit' => 4));

                $photos = "";

                foreach($images as $keyF => $image)
		    	{
		    		$photos .= $image['UserImages']['position'];
		    	}

		    	$message = $visited['User']['name'] . ' ' . $notification['Notification']['body'];

                $passphrase = 'push40st';

                $ctx = stream_context_create();
                stream_context_set_option($ctx, 'ssl', 'local_cert', 'app/Controller/ck.pem');
                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                $body['aps'] = array(
			                            'alert' => array(
			                            					'body' => $message,
			                            					'action-loc-key' => 'Conferir'
			                            				),
			                            'sound' => 'default',
			                            'badge' => '1',
			                            'matcher' => $id_notifier,
			                            'matched' => $id_notified,
			                            'idfacebook' => $visited['User']['facebook_id'],
			                            'photos' => $photos,
			                            'name' => $visited['User']['first_name'],
			                            'desc' => $visited['User']['description'],
			                            'age' => $visited['User']['birthday'],
			                            'dist' => round($visiter[0]['distance'])
                        			);

                $payload = json_encode($body);

                foreach($tokens as $keyT => $token)
                {
                    $deviceToken = $token['DeviceInfos']['device_token'];

                    $fp = stream_socket_client(
                            'ssl://gateway.sandbox.push.apple.com:2195', $err,
                            $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

                    if (!$fp)
                            exit("Falha na conexao: $err $errstr" . PHP_EOL);

                    echo 'Conectado ao APNS' . PHP_EOL;

                    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                    $result = fwrite($fp, $msg, strlen($msg));

                    if (!$result)
                            $json = array('status' => 500);
                    else
                            $json = array('status' => 200);

                    fclose($fp);                                                               

            		echo json_encode($json);
                }   

                $this->Notification->id = $notification['Notification']['id'];
                $this->Notification->delete();
            }
        }

        public function visitperfil()
        {
        	if($this->request->is('post'))
			{

				App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
			    $facebook = new Facebook(array(
			      'appId'  => Configure::read("FB_APP_ID"),
			      'secret' => Configure::read("FB_APP_SECRET"),
			    ));

			    $user = $this->request->data;
				$this->User->id = $user['User']['matched'];

				$matched = $this->User->read();

				$facebook->setAccessToken($matched['User']['facebook_token']);
				
				$commons = $facebook->api('/' . $user['User']['matcher'] . '?fields=context.fields(mutual_likes),context.fields(mutual_friends)');
					    	
				$result['User']['commons'] = $commons;

        		$json = array('status' => 200, 'result' => $result);
			}
			else
			{
				$json = array('status' => 500);
			}
        	
        	echo json_encode($json);
        }

        public function returnmatches()
        {
        	if($this->request->is('post'))
			{
				$user = $this->request->data;

	        	$this->loadModel('Match');

				$matches = array();

	        	$results = $this->Match->find('all', array(
	        												'conditions' => array(
	        																		'OR' => array(
	        																						'Match.matcher' => $user['User']['id'],
	        																						'Match.matched' => $user['User']['id']
														   										  )
	        																	  )
	        											   )
	        								  );

	        	foreach($results as $keyM => $result)
	        	{
	        		if($result['Match']['matcher'] == $user['User']['id'])
	        		{
	        			$match = $this->User->find('first', array('fields' => array(
	        																		'id',
	        																		'first_name',
	        																		'last_name',
	        																		'facebook_id'
	        																	   ),
	        													  'conditions' => array(
	        													 						'User.id' => $result['Match']['matched']
	        													 					  )
	        													 ));

	        			$matches[] = array(
	        								'id' => $match['User']['id'],
	        								'match_id' => $result['Match']['id'],
	        								'name' => $match['User']['first_name'] . " " . $match['User']['last_name'],
	        								'facebook' => $match['User']['facebook_id']
	        							  );
	        		}

	        		if($result['Match']['matched'] == $user['User']['id'])
	        		{
	        			$match = $this->User->find('first', array('fields' => array(
	        																		'id',
	        																		'first_name',
	        																		'last_name',
	        																		'facebook_id'
	        																	   ),
	        													  'conditions' => array(
	        													 						'User.id' => $result['Match']['matcher']
	        													 					  )
	        													 ));

	        			$matches[] = array(
	        								'id' => $match['User']['id'],
	        								'match_id' => $result['Match']['id'],
	        								'name' => $match['User']['first_name'] . " " . $match['User']['last_name'],
	        								'facebook' => $match['User']['facebook_id']
	        							  );
	        		}
	        	}

	        	$json = array('status' => 200, 'matches' => $matches);
	        }
	        else
	        {
	        	$json = array('status' => 500);
	        }

	    	echo json_encode($json);
        }

        public function dounmatch()
        {
        	if($this->request->is('post'))
			{
				$user = $this->request->data;

	        	$this->loadModel('Match');

				$match = $this->Match->find('first', array(
															'conditions' => array(
	        																		'OR' => array(
	        																						array(
	        																								'AND' => array(
			        																										'Match.matcher' => $user['Match']['unmatcher'],
			        																										'Match.matched' => $user['Match']['unmatched']
			        																									   )
	        																							  ),
	        																						array(
			        																						'AND' => array(
			        																										'Match.matcher' => $user['Match']['unmatched'],
			        																										'Match.matched' => $user['Match']['unmatcher']
																														   )
	        																							  )
	        																					  )
	        																	  )
	        											   	)
	        								  );

				$this->Match->id = $match['Match']['id'];

				$this->Match->delete();
				
        		$json = array('status' => 200);	
	        }
	        else
	        {
	        	$json = array('status' => 500);
	        }

	    	echo json_encode($json);
        }

		public function teste()
		{
			/*
			App::import('Vendor', 'facebook', array('file' => 'facebook/facebook.php'));    
		    $facebook = new Facebook(array(
		      'appId'  => Configure::read("FB_APP_ID"),
		      'secret' => Configure::read("FB_APP_SECRET"),
		    ));

		    $facebook->setAccessToken('CAAUu5an6RjUBAO3WSZB1cPB0vsAIAMSkoYOVCga5ZB6WyPkXTQtjRpKCl6aREhsEMuwbEfd9m6Swm8xUFN1KCavCpuz1b9pGoTlrYdsL9OHtK6M9nS6dnkidtCsdesJs3qvOop4ThfP64PAiG0K5PyZC21sOftmPivKXzr5sUZAqhvrmM1HZA1C9iZALQGBCCnP0eBdh3cSvvic7dL0yx5');
		    */
			$this->loadModel('Match');

			$match = $this->Match->find('first', array(
    													'conditions' => array(
        																		'OR' => array(
        																						array(
        																								'AND' => array(
		        																										'Match.matcher' => 590,
		        																										'Match.matched' => 8
		        																									   )
        																							  ),
        																						array(
		        																						'AND' => array(
		        																										'Match.matcher' => 8,
		        																										'Match.matched' => 590
																													   )
        																							  )
        																					  )
        																	  )
        											   	)
        								  );

			$this->Match->id = $match['Match']['id'];

			if($this->Match->delete())
			{
        		$json = array('status' => 200);					
			}
			else
			{
				$json = array('status' => 500);
			}

	    	echo json_encode($match);     	

	        //var_dump($searchs);

	    	//$json = array('status' => 200, 'result' => $result);	    	

			/*$file = 'app/webroot/image/searchuser.txt';
			$post = json_encode($inter_values);

			$file = fopen($file, 'a');
			fwrite($file, $post);
			fclose($file);*/

        	$log = $this->User->getDataSource()->getLog(false, false);
			debug($log);
		}

		public function status($id = null)
		{
			if(isset($id))
			{
				$userInteration = $this->UserInteration->hasAny(array('UserInteration.user_id' => $id, '(TIME_TO_SEC(TIMEDIFF(now(),  UserInteration.created))/60) <' => 10));
				if($userInteration)
				{
					echo json_encode(array('status' => '200', 'message' => 'online'));
					
					return true;
				}

			}

			echo json_encode(array('status' => '200', 'message' => 'offline'));

			return false;		
		}	
	}
?>
