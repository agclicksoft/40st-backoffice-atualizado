<?php
App::uses('FrontendsController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
class ClientsController extends FrontendsController 
{
    public $uses = array('User');
    public $layout = 'add';
    public $helpers = array('Js' => array('Jquery'),'Html', 'Form', 'Session');
    public $components = array('Session');

    public function beforeFilter() 
    {
        parent::beforeFilter();
        //$this->Auth->allow('add','sucess','index','delete','testemail');
    }

    public function add() 
    {
        $this->loadModel('Country');
    	$data = $this->Country->find('list', array('fields' => array('id','name')));
        $this->set('countries', $data);
    	
    	$this->loadModel('State');
    	$data = $this->State->find('list', array('fields' => array('id','name')));
        $this->set('states', $data);

        //Carregando as informação para o login com o facebook
        if(isset($this->params['url']['facebook_id']))
            $this->set('facebook_id', $this->params['url']['facebook_id']);

        if(isset($this->params['url']['save']))
            $this->set('save', $this->params['url']['save']);
        
        
        if(isset($this->params['url']['facebook_token']))
            $this->set('facebook_token', $this->params['url']['facebook_token']);

        if(isset($this->params['url']['name']))
            $this->set('name', $this->params['url']['name']);

        if(isset($this->params['url']['email']))
            $this->set('email', $this->params['url']['email']);   

        if(isset($this->params['url']['facebook_token']))
            $this->set('facebook_token', $this->params['url']['facebook_token']);

        if(isset($this->params['url']['first_name']))            
            $this->set('first_name', $this->params['url']['first_name']);
       
        if(isset($this->params['url']['last_name']))
            $this->set('last_name', $this->params['url']['last_name']);

        if(isset($this->params['url']['birthday']))
        {
            $birthday =  split ("-", $this->params['url']['birthday']); 
            if(count($birthday) > 1)         
                $this->set('birthday', $birthday[2] . "/" . $birthday[1] . "/" . $birthday[0]);         
        }

        if(isset($this->params['url']['gender']))
        {
            if($this->params['url']['gender'] == "male")        
                $this->set('gender', 1);
            else
                $this->set('gender', 2);        
        }
    	
    	if ($this->request->is('post')) 
        {
            $this->User->create();

            $user = $this->request->data;

			$user['User']['name'] = $user['User']['first_name'] . ' ' . $user['User']['last_name'];
            
            if(isset($user['User']['birthday']))
            {
                $birthday =  split ("/", $user['User']['birthday']);
                $user['User']['birthday'] = $birthday[2] . "-" . $birthday[1] . "-" . $birthday[0];
            }
            
            if (isset($user['User']['facebook_id']))
                $user['User']['valid_login'] =  true;
            else
                $user['User']['valid_login'] =  false;
            
            $save = true;
            $message = ""; 

            if(isset($user['User']['facebook_id']))
            {
                $conditions = array('OR' => array('User.facebook_id' => $user["User"]["facebook_id"], 'User.email like' => $user["User"]["email"]));
                if ($this->User->hasAny($conditions))
                {
                    $save = false;
                    $message = "Já existe um usuário cadastrado com esse e-mail ou facebook";
                }
            }    
            else
            {                   
                $conditions = array('User.email like ' => $user["User"]["email"]);                    
                if ($this->User->hasAny($conditions))
                {
                    $save = false;
                    $message = "Já existe um usuário cadastrado com esse e-mail";
                }
            }      

            if($save)
            {                
                if($this->User->save($user)) 
                {
                    if(isset($user['User']['facebook_id']))
                        $this->set('facebook_id', $user['User']['facebook_id']);

                    if(isset($user['User']['facebook_token']))
                        $this->set('facebook_token', $user['User']['facebook_token']);

                    if(isset($user['User']['gender']))
                        $this->set('gender', $user['User']['gender']);


                    if($user['User']['valid_login'])
                    {
                        //$this->Session->setFlash(__('Suas informações foram salvas em nossa base de dados.'));                         

                        $Email = new CakeEmail('smtp');
                       
                        $user = $this->User->find('first',array('conditions' => array('User.id' => $this->User->id))); 

                        $Email->viewVars(array(
                            'nome' => $user['User']['first_name'] . ' ' . $user['User']['last_name'],
                            'id_user' => $user['User']['salt'],
                            'origem' => "facebook"
                        ));

                        $Email->template('confirm', 'email_layout')
                        ->emailFormat('html')
                        ->to( $user['User']['email'])
                        ->subject('Confirme o seu cadastro no 40STREET')
                        ->send(); 
						
						
                        //return $this->redirect(array('action' => 'add', '?' => array('facebook_id' => $user['User']['facebook_id'],'save' => 1)));
                    }
                    else
                    {
                        //$this->Session->setFlash(__('Em breve você receberá um e-mail de validação do seu cadastro.'));
                        $Email = new CakeEmail('smtp');

                        $user = $this->User->find('first',array('conditions' => array('User.id' => $this->User->id))); 

                        $Email->viewVars(array(
                            'nome' => $user['User']['first_name'] . ' ' . $user['User']['last_name'],
                            'id_user' => $user['User']['salt'],
                            'origem' => "no_facebook"
                        ));

                        $Email->template('confirm', 'email_layout')
                        ->emailFormat('html')
                        ->to( $user['User']['email'])
                        ->subject('Confirme o seu cadastro no 40STREET')
                        ->send(); 
                        //return $this->redirect(array('action' => 'add', '?' => array('save' => 1)));
                    }  

                    $this->set('save', 1);                   
                }
            }
            else
            {    
                if(isset($user['User']['facebook_id']))
                    $this->set('facebook_id', $user['User']['facebook_id']);

                if(isset($user['User']['facebook_token']))
                    $this->set('facebook_token', $user['User']['facebook_token']);

                if(isset($user['User']['gender']))
                    $this->set('gender', $user['User']['gender']);

                $this->Session->setFlash(__($message));
            }               
        }

        //$this->layout = 'add';
    }

	

    public function sucess($id = null)
    {                     
       $user = $this->User->find('first',array('conditions' => array('User.salt' => $id)));   
       $origem = $this->request->query['origem'];         
        if($user)
        {
            if(null !==($this->User->id = $user['User']['id']))
            {
                $this->User->saveField('valid_login', true);
                $this->User->saveField('salt', md5(uniqid(mt_rand(), true)));
            }

            $Email = new CakeEmail('smtp');  

	        $Email->viewVars(array(
            	'nome' => $user['User']['first_name'],
            	'id_user' => $user['User']['salt']
            ));
          
            $Email->template('welcome', 'email_layout')
                ->emailFormat('html')
                ->to($user['User']['email'])
                ->subject('Bem-Vindo ao 40street!')
                ->send();
            
            if($origem == "facebook"){
                $this->redirect('http://www.40street.com.br/confirmado_face.html');
            }
            else{
                $this->redirect('http://www.40street.com.br/confirmado.html');
            }
            
        }
        else
        {
            if($origem == "facebook"){
                $this->redirect('http://www.40street.com.br/confirmado_face.html');
            }
            else{
                $this->redirect('http://www.40street.com.br/confirmado.html');
            }
        }
        //$this->layout = 'add';
    }

    public function delete($id = null)
    {
        $user = $this->User->find('first',array('conditions' => array('User.salt' => $id)));
        if($user)
        {
            if(null !==($this->User->id = $user['User']['id']))
            {
                if (!$this->User->exists())
                {
                  $this->redirect('http://www.40street.com.br/');
                }
                
                if ($this->User->delete())
                {
                  $this->redirect('http://www.40street.com.br/');
                }
            }
        }
        else
        {
             $this->redirect('http://www.40street.com.br/');
        }
    }

    public function index()
    {
        //echo 'aqui';
        //$this->layout = 'teste';
    } 

    public function testemail()
    {
        $Email = new CakeEmail('smtp');        
        $Email->to('app@40street.com.br');
        $Email->subject('Assunto');        
        $Email->send('Mensagem');
    }   
}
?>
