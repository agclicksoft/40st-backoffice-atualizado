<?php
App::uses('BackendsController', 'Controller');
class ReportsController extends  BackendsController
{   
  	
	public $helpers = array('Html','Form','Js' => array('Jquery'));
	public $layout = 'index';

	public function beforeFilter() 
    {
        parent::beforeFilter();
    }

	public function denuncia()
	{
       	$this->loadModel('Optreport');
      
		$joins = array(
		    array(
				'table' => 'users',
				'alias' => 'People',
				'type' => 'INNER',
				'conditions' => array('Report.reported_id = People.id',	)
			),
			array(
				'table' => 'users',
				'alias' => 'User',
				'type' => 'INNER',
				'conditions' => array('Report.reporter_id = User.id',)
			),
			array(
				'table' => 'optreports',
				'alias' => 'Optreports',
				'type' => 'INNER',
				'conditions' => array('Report.opt_report = Optreports.id',)
			)
		);

        $conditions = array();
                 
	    if ($this->request->is('post')) 
	    {
			$search = $this->request->data;      

			if(isset($search['Search']['reported']))
			{
				$conditions['People.name like'] = $search['Search']['reported']."%";
			}

			if(isset($search['Search']['reporter']))
			{
				$conditions['User.name like'] = $search['Search']['reporter']."%";
			}

			if(!empty($search['Search']['optreport']) && $search['Search']['optreport'] != "")
			{
				$conditions['Report.opt_report'] = $search['Search']['optreport'];
			}	
	    } 
		
		$options = array(
			'conditions' => $conditions, 
			'joins'=> $joins,
			'fields' => array('Report.id, People.name,People.id, People.situation, User.name, User.id, User.situation, User.modified, Optreports.report, Report.description, Report.created'),
			'order' => 'Report.created DESC',
			'limit' => 10
		);

		$this->paginate = $options;

		$users = $this->paginate('Report');
		 
		/*$this->paginate = array('conditions' => $conditions,'joins'=>$joins,'fields' => array
		('Report.id, People.name,People.id, People.situation, User.name, User.id, User.situation, User.modified, Optreports.report, Report.description, Report.created'),
		'order' => 'Report.created DESC');
		$this->Report->recursive = -1;*/

		//var_dump($this->paginate());
		//exit;

		$optReport = $this->Optreport->find('list', array('fields' => array('Optreport.id', 'Optreport.report')));

		//var_dump($optReport);
		$this->set('optreport', $optReport);
		$this->set('reports', $this->paginate());
    }

	

	public function delete($id = null) 
	{
	    
        

		$report = $this->Report->findById($id);
		
		$this->loadModel('User');
		
		if(null !==($this->User->id = $report['Report']["reported_id"]))
	    	$this->User->Savefield('situation', false);
		
	 
	    if (!$this->request->is('post'))
	    {
	      throw new MethodNotAllowedException();
	    }
	    
	    $this->Report->id = $id;
	    
	    if (!$this->Report->exists())
	    {
	      throw new NotFoundException(__('Usuário Invalido'));
	    }
	    
	    if ($this->Report->delete())
	    {
	      $this->Session->setFlash(__('Denuncia Deletado'));
	      $this->redirect(array('action' => 'denuncia'));
	    }

	    $this->Session->setFlash(__('Denuncia não foi Deletado'));
	    $this->redirect(array('action' => 'denuncia'));
	 }

	public function block($id = null)
	{

		$this->loadModel('User');
		
		if(null !==($this->User->id = $id))
	    $this->User->Savefield('situation', true);
		$this->redirect(array('action' => 'denuncia'));
	}

    public function unblock($id = null)
	{

		$this->loadModel('User');
		
		if(null !==($this->User->id = $id))
	    $this->User->Savefield('situation', false);
		$this->redirect(array('action' => 'denuncia'));
	}    
}

?>