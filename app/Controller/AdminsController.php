<?php
App::uses('BackendsController', 'Controller');
class AdminsController extends BackendsController
{
  
  public $name = 'Admins';
  public $uses = array('Admin');
  //public $components = array('Security');
  public $layout = 'index';
  //$this->layout = 'index';
	public function beforeFilter() 
  {
      parent::beforeFilter();
  }
 

  public function login() 
  {
    if ($this->request->is('post')) 
    {
       // debug(AuthComponent::password($this->request->data['Admin']['password']));
        $this->request->data['Admin']['password'] = AuthComponent::password($this->request->data['Admin']['password']);
       // debug($this->request->data['Admin']['password']);
        if ($this->Auth->login($this->request->data['Admin']))
        {
            //return $this->redirect($this->Auth->redirect());
            return $this->redirect(array('controller' => 'user_backs', 'action' => 'index'));
        }

        $this->Session->setFlash(__('Usuário ou Senha inválida, tente de novo'));
    }
    $this->layout = 'login';
  }

  public function logout() 
  {
    $this->redirect($this->Auth->logout());
  }



  public function index() 
  {
    
    $conditions = array();

    if ($this->request->is('post')) 
    {
      $search = $this->request->data;      
      
      if(isset($search['Search']['name']))
      {
        $conditions['Admin.name like'] = $search['Search']['name']."%";
      }

      if(isset($search['Search']['login']))
      {
        $conditions['Admin.username like'] = $search['Search']['login']."%";
      }

    }

    //var_dump($conditions);
    //$this->Admin->recursive = 0;
    //$admins = $this->Admin->find('all', array('conditions' => $conditions));

    $options = array(
        'conditions' => $conditions, 
        'order' => array('Admin.created' => 'desc'),
        'limit' => 10
    );

    $this->paginate = $options;

    $admins = $this->paginate('Admin');
    
    $this->set('admins',  $admins);
    //$this->layout = 'index';
  }

  public function add() 
  {
    if ($this->request->is('post')) 
    {
      $this->Admin->create();
      if ($this->Admin->save($this->request->data)) 
      {
        $this->Session->setFlash(__('O usuário foi salvo'));
        $this->redirect(array('action' => 'index'));
      }
      else 
      {
        $this->Session->setFlash(__('O nome não pode ser salvo.Por favor tente de novo.'));
      }
    }
   //this->layout = 'index';
  }


   public function edit($id = null) 
    {
      if (!$id) 
      {
        throw new NotFoundException(__('Invalid post'));
      }

      $admin = $this->Admin->findById($id);
       if (!$admin)
        {
        throw new NotFoundException(__('Invalid post'));
        }

    if ($this->request->is(array('post', 'put'))) 
    {
        $this->Admin->id = $id;

     
       if ($this->Admin->save($this->request->data)) 
        {
            $this->Session->setFlash(__('Editado com sucesso'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Não foi possivel editar.'));
    }
    

    if (!$this->request->data) 
    {
        $this->request->data = $admin;
    }
   // $this->layout = 'index';
 }
                
               
  public function delete($id = null) 
  {
    if (!$this->request->is('post'))
    {
      throw new MethodNotAllowedException();
    }
    
    $this->Admin->id = $id;
    
    if (!$this->Admin->exists())
    {
      throw new NotFoundException(__('Usuário Invalido'));
    }
    
    if ($this->Admin->delete())
    {
      $this->Session->setFlash(__('Usuário Deletado'));
      $this->redirect(array('action' => 'index'));
    }

    $this->Session->setFlash(__('Usuário não foi Deletado'));
    $this->redirect(array('action' => 'index'));
  }

  public function register()
  {
         
        $isPost = $this->request->isPost();
         
        if($isPost && !empty($this->request->data))
        {
             
            $this->request->data['Admin']['password'] = Security::hash($this->request->data['Admin']['password'], null, true);
            $last = $this->Admin->save($this->request->data);
             
        }
         
  }

 /* public function lista() {
                          
                                $options = array
                                (
                                     'fields' => array('Admin.username', 'Admin.name'),
                                     'operator' => 'LIKE',
                                     'value' => array
                                     (
                                     'before' => '%', // opcional
                                     'after' => '%', // opcional
                                     ),
                          
                                    'order' => array('Admin.created' => 'DESC')
                                    'limit' => 20
                                 );
                          
                                $this->paginate = $options;
                          
                                 // Roda a consulta, já trazendo os resultados paginados
                                 $admins = $this->paginate('Admin');
                          
                                  // Envia os dados pra view
                                  $this->set('admins', $admins);
                              } */
                           
                          

}

?>