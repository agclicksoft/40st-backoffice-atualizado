<?php
	App::uses('FrontendsController', 'Controller');
	class NotificationController extends FrontendsController
	{
		public $layout = false;		
		public $name = 'Notification';
		public $uses = array('UserInteration', 'User', 'Notification', 'DeviceInfos', 'UserImages'); 
		public $components = array('Session');

		public function beforeFilter() 
	    {
	        parent::beforeFilter();
	    }
		
		
		public function index()
		{
			$pendents = $this->Notification->find('all', 
				array(
					'limit' => 10, 
					'conditions' => array("Notification.status" => "not_send"),
					'group' => array("Notification.notified", "Notification.notifier", "Notification.type")
				)
			);
			//echo "<pre>";
			//print_r($pendents);
			//exit;

            foreach ($pendents as $keyP => $notification)
            {
                $id_notified = $notification['Notification']['notified'];
                $id_notifier = $notification['Notification']['notifier'];

                switch($notification["Notification"]["type"])
                {
                	case 'chat':
                		//$this->Notification->sendPushChat($notification["Notification"]["id"], json_decode($notification["Notification"]["body"]));
                		$this->Notification->sendPushChat($notification["Notification"]["id"], $notification["Notification"]["body"]);
                		break;
                	default:
                		break;
                }             
            }

            $this->render(false);
        }        	
	}
?>